function pptrials = processInstantaneousEyeMovementsFV(...
    pptrials, pptrials_orig, pixel2angle, dist2monitor, baseline, r_cyc, theta_offset, winsize)

if ~exist('winsize', 'var')
    winsize = 31;
end

% rotation matrix for helmholtz coordinate system
yh = baseline(:) / norm(baseline);
xh = ([1 0 0]' - ([1 0 0]*yh) * yh) / norm(([1 0 0]' - ([1 0 0]*yh) * yh));
zh  = cross(yh, xh);
R_hh = [xh(:), yh(:), zh(:)];

% orthonormal transformation to relative vergence and version components
% version horizontal - vertical, vergence horizontal - vertical
%R_orthonormal = [1, 1; 1, -1] * sqrt(2) / 2;
R_orthonormal = [1 0 1 0; 0 1 0 1; 1 0 -1 0; 0 1 0 -1];

for ii = size(pptrials, 1):-1:1 % reverse for plotting
    %% nanout
    flds = {'blinks', 'notracks'};
    
    baddata = false(pptrials{ii, 1}.samples, 1);
    for kk = 1:2
         for fi = 1:length(flds)
             st = pptrials{ii, kk}.(flds{fi});
             sss = st.start;
             eee = sss + st.duration - 1;
             for si = 1:length(sss)
                 baddata(sss(si):eee(si)) = true;
             end
         end
    end
    pptrials{ii, 1}.baddata = baddata;
    
    %% account for recalibration offsets
    % (overshoots removed)
    x1 = pptrials{ii, 1}.x.position + pixel2angle * 0; %pptrials{ii,1}.RecalOffsetX1;
    y1 = pptrials{ii, 1}.y.position + pixel2angle * 0; %pptrials{ii,1}.RecalOffsetY1;
    
    x2 = pptrials{ii, 2}.x.position + pixel2angle * 0; %pptrials{ii,1}.RecalOffsetX2;
    y2 = pptrials{ii, 2}.y.position + pixel2angle * 0; %pptrials{ii,1}.RecalOffsetY2;
    
    x1_sm0 = sgfilt(x1, 3, winsize, 0);
    y1_sm0 = sgfilt(y1, 3, winsize, 0);
    x2_sm0 = sgfilt(x2, 3, winsize, 0);
    y2_sm0 = sgfilt(y2, 3, winsize, 0);
    
    % eye velocities
    x1_sm1 = sgfilt(x1, 3, winsize, 1);
    y1_sm1 = sgfilt(y1, 3, winsize, 1);
    x2_sm1 = sgfilt(x2, 3, winsize, 1);
    y2_sm1 = sgfilt(y2, 3, winsize, 1);
    vel1 = sqrt(x1_sm1.^2 + y1_sm1.^2) * 1000;
    vel2 = sqrt(x2_sm1.^2 + y2_sm1.^2) * 1000;
    
    % eye curvatures
    curvature1 = abs(cur(x1_sm0, y1_sm0));
    curvature2 = abs(cur(x2_sm0, y2_sm0));
   
    % (with overshoots)
    x1_orig = pptrials_orig{ii, 1}.x.position + pixel2angle * 0; %pptrials{ii,1}.RecalOffsetX1;
    y1_orig = pptrials_orig{ii, 1}.y.position + pixel2angle * 0; %pptrials{ii,1}.RecalOffsetY1;
    
    x2_orig = pptrials_orig{ii, 2}.x.position + pixel2angle * 0; %pptrials{ii,1}.RecalOffsetX2;
    y2_orig = pptrials_orig{ii, 2}.y.position + pixel2angle * 0; %pptrials{ii,1}.RecalOffsetY2;
    
    
    
    
    %% use unfiltered traces
%     x1 = x1;
%     y1 = y1;
%     x2 = x2;
%     y2 = y2;
    
    %% compute vergence and version in both coordinate systems
    % add offset to DPI coordinates
    theta_1h = sgfilt(deg2rad(x1/60) + theta_offset(1, 1), 3, winsize, 0); 
    theta_1v = sgfilt(deg2rad(y1/60) + theta_offset(1, 2), 3, winsize, 0);
    theta_2h = sgfilt(deg2rad(x2/60) + theta_offset(2, 1), 3, winsize, 0);
    theta_2v = sgfilt(deg2rad(y2/60) + theta_offset(2, 2), 3, winsize, 0);
    
    theta_1h = nanout(baddata, theta_1h);
    theta_1v = nanout(baddata, theta_1v);
    theta_2h = nanout(baddata, theta_2h);
    theta_2v = nanout(baddata, theta_2v);
    
    % unit gaze vectors from each eye
    u_R = getUfromAzimuthAndElevation(theta_1h, theta_1v);
    u_L = getUfromAzimuthAndElevation(theta_2h, theta_2v);
    
    [r_bgp, s_R, s_L] = findBinocularGazePoint(baseline, u_R, u_L, r_cyc);
    
    % intersection of binocular gaze point with monitor
    r_bgp_int = bsxfun(@rdivide, r_bgp, r_bgp(1, :)) * dist2monitor;
    
    % unit vectors of each line of sight in Helmholtz coordinates (head
    % coordinates)
    u_R_hh = u_R' * R_hh;
    u_L_hh = u_L' * R_hh;
    
    % helmoltz angles of each eye (eqns 8.16 and 8.17)
    [hh1_R, hh2_R] = getHelmholtzAngles(u_R_hh);
    [hh1_L, hh2_L] = getHelmholtzAngles(u_L_hh);
    
    V_h = hh2_R - hh2_L; % absolute vergence
    V_v = hh1_R - hh1_L;
    
    %% get version and vergence components of raw data
    vs = [x1(:), y1(:), x2(:), y2(:)] * R_orthonormal;
    vs_sm0 = nan(size(vs));
    vs_sm1 = nan(size(vs));
    for jj = 1:4
        vs_sm0(:, jj) = sgfilt(vs(:, jj), 3, winsize, 0); 
        vs_sm1(:, jj) = sgfilt(vs(:, jj), 3, winsize, 1);
    end
    
    vs_sm0(baddata, :) = nan;
    vs_sm1(baddata, :) = nan;
    
    pptrials{ii, 1}.vs_components = vs_sm0;
    pptrials{ii, 1}.vs_velocities = vs_sm1;
    
    %% nanout and save
    pptrials{ii, 1}.x.shifted_unsm = nanout(baddata, x1);
    pptrials{ii, 1}.y.shifted_unsm = nanout(baddata, y1);
    pptrials{ii, 2}.x.shifted_unsm = nanout(baddata, x2);
    pptrials{ii, 2}.y.shifted_unsm = nanout(baddata, y2);
    
    pptrials{ii, 1}.x.shifted = nanout(baddata, x1_sm0);
    pptrials{ii, 1}.y.shifted = nanout(baddata, y1_sm0);
    pptrials{ii, 2}.x.shifted = nanout(baddata, x2_sm0);
    pptrials{ii, 2}.y.shifted = nanout(baddata, y2_sm0);
    
    pptrials{ii, 1}.x.velocity_sm = nanout(baddata, x1_sm1);
    pptrials{ii, 1}.y.velocity_sm = nanout(baddata, y1_sm1);
    pptrials{ii, 2}.x.velocity_sm = nanout(baddata, x2_sm1);
    pptrials{ii, 2}.y.velocity_sm = nanout(baddata, y2_sm1);
    pptrials{ii, 1}.velocity_sg = nanout(baddata, vel1);
    pptrials{ii, 2}.velocity_sg = nanout(baddata, vel2);
    
    pptrials{ii, 1}.curvature = nanout(baddata, curvature1);
    pptrials{ii, 2}.curvature = nanout(baddata, curvature2);
    
    pptrials{ii, 1}.x.orig = nanout(baddata, x1_orig);
    pptrials{ii, 1}.y.orig = nanout(baddata, y1_orig);
    pptrials{ii, 2}.x.orig = nanout(baddata, x2_orig);
    pptrials{ii, 2}.y.orig = nanout(baddata, y2_orig);
    
    pptrials{ii, 1}.theta = [theta_1h(:),theta_1v(:), theta_2h(:), theta_2v(:)];
    pptrials{ii, 1}.u_R = u_R;
    pptrials{ii, 1}.u_L = u_L;
    pptrials{ii, 1}.u_R_hh = u_R_hh;
    pptrials{ii, 1}.u_L_hh = u_L_hh;
    pptrials{ii, 1}.hh = [hh2_R(:), hh1_R(:), hh2_L(:), hh1_L(:)];
    pptrials{ii, 1}.V_abs = [V_h(:), V_v(:)];
    pptrials{ii, 1}.dist_loca = [s_R(:), s_L(:)];
    pptrials{ii, 1}.r_bgp = r_bgp;
    pptrials{ii, 1}.r_bgp_int = r_bgp_int;
    
    %% check if calibration was skipped on this trial
    if ii > 1
        pptrials{ii, 1}.RecalOffsetX1_prev = pptrials{ii-1, 1}.RecalOffsetX1;
        pptrials{ii, 1}.RecalOffsetY1_prev = pptrials{ii-1, 1}.RecalOffsetY1;
        pptrials{ii, 1}.RecalOffsetX2_prev = pptrials{ii-1, 1}.RecalOffsetX2;
        pptrials{ii, 1}.RecalOffsetY2_prev = pptrials{ii-1, 1}.RecalOffsetY2;
        
        if pptrials{ii, 1}.RecalOffsetX1_prev == pptrials{ii, 1}.RecalOffsetX1 && ...
                pptrials{ii, 1}.RecalOffsetY1_prev == pptrials{ii, 1}.RecalOffsetY1 && ...
                pptrials{ii, 1}.RecalOffsetX2_prev == pptrials{ii, 1}.RecalOffsetX2 && ...
                pptrials{ii, 1}.RecalOffsetY2_prev == pptrials{ii, 1}.RecalOffsetY2
            pptrials{ii, 1}.skippedCalibration = true;
        else 
            pptrials{ii, 1}.skippedCalibration = false;
        end
    else
        pptrials{ii, 1}.RecalOffsetX1_prev = -inf;
        pptrials{ii, 1}.RecalOffsetY1_prev = -inf;
        pptrials{ii, 2}.RecalOffsetX2_prev = -inf;
        pptrials{ii, 2}.RecalOffsetY2_prev = -inf;
        pptrials{ii, 1}.skippedCalibration = false;
    end
    
    %% plot an example
    if false && ~pptrials{ii, 1}.skippedCalibration
        sreg = 2000;
        
        pp = dist2monitor * tan(deg2rad(pixel2angle) / 60); % pixel pitch
        ll = pp * ((-5:2:5)*5 - .5); % letter locations in mm
        figure(1); clf;
        for ee = 1:6
            patch('YData', ll(ee) + pp*-2.5 * [-1, 1, 1, -1],...
                'ZData', pp*-2.5 * [-1, -1, 1, 1],...
                'XData', dist2monitor*ones(1, 4),...
                'FaceColor', 'k', 'FaceAlpha', .3);
            hold on;
        end
        plot3(r_bgp(1, sreg:end), r_bgp(2, sreg:end), r_bgp(3, sreg:end));
        
        axis image;
        set(gca, 'YDir', 'reverse');
        
        figure(2); clf; hold on;
        plot(x1); plot(y1); plot(x2); plot(y2);
        keyboard;
    end
end
end