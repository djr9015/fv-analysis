close all
%% load in a file from free viewing task
fprintf('Loading file...\n')
load("C:\Users\ryckm_000\Documents\MATLAB\Freeview\FV-analysis\RawDataCopy\MAC_pptrials_processed.mat", 'pptrials'); % load in variable 'pptrials' from mat file
Fs = 1000;                 % this data was recorded on the DPI (1000Hz)

%% aggregate data of saccades
fprintf('Aggregating saccade data...\n')
num_trials = size(pptrials, 1);
ampsR = [];
angsR = [];
for trialIdx = 1:num_trials
    ampsR = [ampsR, pptrials{trialIdx,1}.saccades.amplitude]; % units = arcmin
    angsR = [angsR, pptrials{trialIdx,1}.saccades.angle]; % units = radians
end
ampsL = [];
angsL = [];
for trialIdx = 1:num_trials
    ampsL = [ampsL, pptrials{trialIdx,2}.saccades.amplitude]; % units = arcmin
    angsL = [angsL, pptrials{trialIdx,2}.saccades.angle]; % units = radians
end
figure(1); clf;
subplot(2, 2, 1);
histogram(ampsL, 30);
xlabel('L saccade amplitude (arcmin)');
ylabel('count');

subplot(2, 2, 3);
polarhistogram(angsL, 30);
title('L saccade directions');

subplot(2, 2, 2);
histogram(ampsR, 30);
xlabel('R saccade amplitude (arcmin)');
ylabel('count');

subplot(2, 2, 4);
polarhistogram(angsR, 30);
title('R saccade directions');

%% Drift
fprintf('Analyzing drift...\n')
drifts1 = struct('x', [], 'y', [], 'trialIdx', [], 'driftIdx', []);
drifts2 = struct('x', [], 'y', [], 'trialIdx', [], 'driftIdx', []);
driftsLR = {drifts1, drifts2};

% specify parameters to analyze drifts
smoothing = 31; % smoothing window size (indices)
cutseg = floor(smoothing/2);
maxSpeed = 5 * 60;

drift_pertrial = length(pptrials{1}.drifts.start);
% loop for R/L
for side = 1:2
    di_loop = 0;
    % loop over all trials
    for trialIdx = 1:num_trials
        drift_pertrial = length(pptrials{trialIdx,side}.drifts.start);
        % loop through each drift, save its trace to 'drifts'
        for di = 1:drift_pertrial
            % start and stop times of this drift segment, extract x and y traces
            startIdx = pptrials{trialIdx,side}.drifts.start(di);
            stopIdx = startIdx + pptrials{trialIdx,side}.drifts.duration(di) - 1;

            xdrift = pptrials{trialIdx,side}.x.position(startIdx:stopIdx);
            ydrift = pptrials{trialIdx,side}.y.position(startIdx:stopIdx);
            
            %find mean vergence for given drift (x component only)
            verg_avg = mean(pptrials{trialIdx,1}.vs_components(startIdx:stopIdx, 3));
            
            %drift duration
            driftduration = (stopIdx-startIdx)/Fs;

            if length(xdrift) < smoothing
                continue; 
            end

            % use getDriftChar to measure drift characteristcs
            [span, mn_speed, mn_cur, varx, vary, smx, smy] = ...
                getDriftChar(xdrift, ydrift, smoothing, cutseg, maxSpeed);

            % save data to drifts structure
            di_loop = di_loop + 1; %index of drift including all previous trials
            driftsLR{side}(di_loop).x = smx;
            driftsLR{side}(di_loop).y = smy;
            driftsLR{side}(di_loop).trialIdx = trialIdx;
            driftsLR{side}(di_loop).driftIdx = di_loop;
            driftsLR{side}(di_loop).span = span;
            driftsLR{side}(di_loop).speed = mn_speed;
            driftsLR{side}(di_loop).duration = driftduration;
            driftsLR{side}(di_loop).curv = mn_cur;
            driftsLR{side}(di_loop).varx = varx;
            driftsLR{side}(di_loop).vary = vary;
            driftsLR{side}(di_loop).mean_vergence = verg_avg;
%             drifts{side}(di_loop).count = driftCt;
            
%             if ~isempty(drifts{side}(di_loop).driftIdx)
%                 driftCt = driftCt+1;
%             end
        end
    end
end
%% Plot drift characteristics
% Plot distributions of drift span, speed, curvature, and variances.
for side = 1:2
    figure(1+side); clf;
    sgtitle(sprintf('Channel %d drift data', side));
    subplot(2, 2, 1); % drift span
    histogram([driftsLR{side}.span], 30);
    xlabel('drift span (arcmin)');
    ylabel('count');

    subplot(2, 2, 2); % drift speed
    histogram([driftsLR{side}.speed], 30);
    xlabel('drift speed (arcmin /s)');
    ylabel('count');

    subplot(2, 2, 3); % drift curvature
    histogram([driftsLR{side}.curv], linspace(0, 80, 30));
    xlabel('drift curvature (arcmin^{-1})');
    ylabel('count');
    
    hVarx = histcounts([driftsLR{side}.varx], 30);
    hVary = histcounts([driftsLR{side}.vary], 30);
    maxVarCt = max([hVarx hVary]);
    varlim = ceil(maxVarCt/50)*50;
    
    subplot(4, 2, 6); % horizontal variance
    histogram([driftsLR{side}.varx], 30);
    xlabel('horizontal variance (arcmin^2)');
    ylabel('count');
    ylim([-inf varlim]);

    subplot(4, 2, 8); % vertical variance
    histogram([driftsLR{side}.vary], 30);
    xlabel('vertical variance (arcmin^2)');
    ylabel('count');
    ylim([-inf varlim]);
    
    figure(3+side)
    sgtitle(sprintf('Channel %d drift duration', side));
    histogram([driftsLR{side}.duration], 30)
    xlabel('duration [s]')
    ylabel('count')
    
    figure(5+side)
    sgtitle(sprintf('Channel %d drift mean vergence', side));
    histogram([driftsLR{side}.mean_vergence], 40)
    xlabel('vergence (x)')
    ylabel('count')
end

%% Drift vergence relationships (ch1)
%%{
figpath = "C:\Users\ryckm_000\Documents\MATLAB\Freeview\figures\driftCharacteristics";
close all
dim = [.2 .5 .3 .3];
ch = 1;
figure
scatter([driftsLR{ch}.mean_vergence], [driftsLR{ch}.span], 5, 'filled', 'k')
lsline
xlabel('vergence')
ylabel('span')
title('span')
fitspan = fitlm([driftsLR{ch}.mean_vergence], [driftsLR{ch}.span]);
str = sprintf('R^2 = %f',fitspan.Rsquared.Ordinary);
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftspan_corr.png'))


figure
scatter([driftsLR{ch}.mean_vergence], [driftsLR{ch}.speed], 5, 'filled', 'k')
lsline
title('speed')
xlabel('vergence')
ylabel('speed')
fitspeed = fitlm([driftsLR{ch}.mean_vergence], [driftsLR{ch}.speed]);
str = sprintf('R^2 = %f',fitspeed.Rsquared.Ordinary);
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftspeed_corr.png'))

figure
scatter([driftsLR{ch}.mean_vergence], [driftsLR{ch}.vary], 5, 'filled', 'k')
lsline
title('vert. variance')
xlabel('vergence')
ylabel('variance (y)')
fitvary = fitlm([driftsLR{ch}.mean_vergence], [driftsLR{ch}.vary]);
str = sprintf('R^2 = %f',fitvary.Rsquared.Ordinary);
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftvarx_corr.png'))

figure
scatter([driftsLR{ch}.mean_vergence], filloutliers([driftsLR{ch}.varx], 'nearest', 'mean'), 5, 'filled', 'k')
lsline
title('horiz. variance')
xlabel('vergence')
ylabel('variance (x)')
fitvarx = fitlm([driftsLR{ch}.mean_vergence], [driftsLR{ch}.varx]);
str = sprintf('R^2 = %f',fitvarx.Rsquared.Ordinary);
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftvary_corr.png'))

figure
scatter([driftsLR{ch}.mean_vergence], filloutliers([driftsLR{ch}.curv], 'nearest', 'mean'), 5, 'filled', 'k')
lsline
title('curvature')
xlabel('vergence')
ylabel('curvature')
fitcurv = fitlm([driftsLR{ch}.mean_vergence], [driftsLR{ch}.curv]);
str = sprintf('R^2 = %f',fitcurv.Rsquared.Ordinary);
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftcurv_corr.png'))

figure
scatter([driftsLR{ch}.mean_vergence], [driftsLR{ch}.duration], 7, 'filled', 'k')
lsline
title('duration')
xlabel('vergence')
ylabel('duration')
fitduration = fitlm([driftsLR{ch}.mean_vergence], [driftsLR{ch}.duration]);
str = sprintf('R^2 = %f',fitduration.Rsquared.Ordinary);
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftduration_corr.png'))

%}

%% Drift diffusion constants
fprintf('Calulating diffusion constants...\n')
warning('off')
for side = 1:2
[~, ~, dc, ~, Dsq, SingleSegmentDsq] = ...
    CalculateDiffusionCoef (driftsLR{side});

fprintf('Diffusion constant (channel %d) = %1.2f arcmin^2/s\n', side, dc);

tdsq = (1:length(Dsq)) / Fs * 1000; % ms

figure(7+side); clf; % plot information about drift diffusion
sgtitle(sprintf('Channel %d diffusion data', side));
subplot(1, 2, 1);
plot(tdsq, SingleSegmentDsq, 'lineWidth', .25, 'linestyle', '-');
xlim(tdsq([1, end]));
xlabel('time lag (ms)');
ylabel('variance of displacement (arcmin^2)');
title('single segments');

subplot(1, 2, 2); hold on;
plot(tdsq, Dsq, 'k-', 'linewidth', 2);
plot(tdsq, (4 * tdsq) * dc / Fs, 'b--', 'linewidth', 2)
xlim(tdsq([1, end]));
xlabel('time lag (ms)');
ylabel('variance of displacement (arcmin^2)');
legend({'data', 'brownian motion'}, 'Location', 'northwest');
title('all driftsLR');
end
warning('on')
%% Density plots