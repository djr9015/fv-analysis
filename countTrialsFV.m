function counts = countTrialsFV(pptrials)

stabLim = 10; % arcmin

counts = struct();

counts.notracks = false(size(pptrials, 1), 1);
counts.blinks = false(size(pptrials, 1), 1);
counts.badcalibration = false(size(pptrials, 1), 1);
counts.badstabilization = false(size(pptrials, 1), 1);
counts.exclude = false(size(pptrials, 1), 1);
counts.saccades = false(size(pptrials, 1), 1);
counts.microsaccades = false(size(pptrials, 1), 1);
counts.driftonly = false(size(pptrials, 1), 1);
counts.lowperformance = false(size(pptrials, 1), 1);
counts.monocular = false(size(pptrials, 1), 1);
counts.stabilized = false(size(pptrials, 1), 1);

for ii = 1:size(pptrials, 1)
    
    if  ~isempty(pptrials{ii}.notracks.start) || ...
        isempty(pptrials{ii}.drifts.start)
        counts.notracks(ii) = true;
%         continue;
    end
    if  ~isempty(pptrials{ii}.notracks.start)
        counts.blinks(ii) = true;
%         continue;
    end
    
%     if pptrials{ii, 1}.unstab == 0
%         useidx = ceil((10+pptrials{ii, 1}.timeRampStart)):floor(pptrials{ii, 1}.timeRespStart);
%         
%         x1 = pptrials{ii, 1}.x.orig(useidx);% use original traces with overshoots
%         y1 = pptrials{ii, 1}.y.orig(useidx);
%         x2 = pptrials{ii, 2}.x.orig(useidx);
%         y2 = pptrials{ii, 2}.y.orig(useidx);
%         
%         X1Stab = pptrials{ii, 1}.XStab(useidx);%;
%         Y1Stab = pptrials{ii, 1}.YStab(useidx);%;
%         X2Stab = pptrials{ii, 2}.XStab(useidx);% ;
%         Y2Stab = pptrials{ii, 2}.YStab(useidx);% ;
%         
%         
%         
%         bad = ...
%             abs(x1 - X1Stab) > stabLim | ...
%             abs(y1 - Y1Stab) > stabLim | ...
%             abs(x2 - X2Stab) > stabLim | ...
%             abs(y2 - Y2Stab) > stabLim;
%         
%         counts.badstabilization(ii) = sum(bad) > 10;
%         
%         if counts.badstabilization(ii)
%             continue;
%         end
%     end
%     
    if ~isempty(pptrials{ii, 1}.saccades) || ...
           ~isempty(pptrials{ii, 2}.saccades)
        counts.saccades(ii) = true;
%         continue;
    end
    if ~isempty(pptrials{ii, 1}.microsaccades) || ...
            ~isempty(pptrials{ii, 2}.microsaccades)
        counts.microsaccades(ii) = true;
%         continue;
    end
    counts.driftonly(ii) = true;
    
    if pptrials{ii, 1}.EyeCond ~= 0
        counts.monocular(ii) = true;
    end
    
    if pptrials{ii, 1}.StabCond ~= 3
        counts.stabilized(ii) = true;
    end
end

counts.exclude = counts.blinks | counts.notracks | counts.badstabilization | ...
    counts.monocular | counts.stabilized;
counts.badcalibration = false(size(counts.exclude)); % for now JI TO DO


%% print things
fprintf('Total trials: %i\n', size(pptrials, 1));
fprintf('Valid trials: %i\n', sum(~counts.exclude));
end