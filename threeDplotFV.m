% Runs a single trial from freeview task, creates various plots in 3d
datafile = "C:\Users\ryckm_000\Documents\MATLAB\Freeview\FV-analysis\RawDataCopy\MAC_pptrials_segmented.mat";
%load(datafile, 'pptrials'); % load in variable 'pptrials' from mat file
Fs = 1000; %was this data recorded on the DPI??? (1000Hz)
close all
%% Choose single trial
trialIdx = 2;
%%
% all line plots
%%{
%% 3d plot
num_trials = length(pptrials);
bgp = pptrials{trialIdx,1}.r_bgp_int;

%improve readability with grid and colors (scatter3). 
bgp_plot = figure(1);
grid on

%comet3mod(bgp(1,:),bgp(2,:),bgp(3,:), 0, 5e-5)
C = jet(length(bgp(1,:)));
scatter3(bgp(1,:), bgp(2,:), bgp(3,:), 5, C);
text(double(bgp(1,1)), double(bgp(2,1)), double(bgp(3,1)), 'Start \rightarrow', 'horizontalalignment', 'right');
colormap(C)
caxis([0 6])
bar = colorbar('eastoutside');
xlabel('z')
ylabel('x')
zlabel('y')
title(bar, 'Time [s]')

%% 2d H/V plot
HVtrace = figure(2);
subplot(2,1,1)
hold on
xlabel('time (ms)');
ylabel('position (arcmin)');
legend('location', 'best')
plot(pptrials{trialIdx,1}.x.position, '-r', 'displayName', 'Right x')
plot(pptrials{trialIdx,2}.x.position, '-b', 'displayName', 'Left x')
subplot(2,1,2)
hold on
xlabel('time (ms)');
ylabel('position (arcmin)');
legend('location', 'best')
plot(pptrials{trialIdx,1}.y.position, '-r', 'displayName', 'Right y')
plot(pptrials{trialIdx,2}.y.position, '-b', 'displayName', 'Left y')

%% Version and vergence
VScomponents = figure(3);
subplot(2,1,1)
hold on
plot(pptrials{trialIdx,1}.vs_components(:,1), '-r', 'displayName', 'Version x')
plot(pptrials{trialIdx,1}.vs_components(:,2), '-g', 'displayName', 'Version y')
legend('location', 'best')
subplot(2,1,2)
hold on
yline(0, 'HandleVisibility', 'off');
plot(pptrials{trialIdx,1}.vs_components(:,3), '-b', 'displayName', 'Vergence x')
plot(pptrials{trialIdx,1}.vs_components(:,4), '-k', 'displayName', 'Vergence y')
legend('location', 'best')

%% Right/Left XY-trace on image
pixel2angle = pptrials{end, 1}.PixelAngle;
resScale = 720/1080;
angle2img = resScale/pixel2angle;
imgpath = "C:\Users\ryckm_000\Documents\MATLAB\Freeview\images\";

% Right
imgname = sprintf('rImage%03dV.bmp', pptrials{trialIdx,1}.ImageNum);
img = imread(fullfile(imgpath, imgname));
figure
imshow(img)
[rows, columns, ~] = size(img);
origin = [columns/2, rows/2];
xdata = -origin(1) : columns - origin(1);
ydata = -origin(2) : rows - origin(2);
imshow(img, 'XData', xdata, 'YData', ydata)
axis off
hold on
x_plot = pptrials{trialIdx,1}.x.position*angle2img;
y_plot = -pptrials{trialIdx,1}.y.position*angle2img;
color = jet(length(x_plot));
clim = [0, length(x_plot)/Fs];
scatter(x_plot, y_plot, 5, color)
text(double(x_plot(1)),double(y_plot(1)), '\leftarrow Start', 'color', 'white')
colormap(color)
caxis(clim)
h = colorbar('southoutside');
title(h, 'Time [s]')

%Left
imgname = sprintf('lImage%03dV.bmp', pptrials{trialIdx,1}.ImageNum);
img = imread(fullfile(imgpath, imgname));
figure
imshow(img)
[rows, columns, ~] = size(img);
origin = [columns/2, rows/2];
xdata = -origin(1) : columns - origin(1);
ydata = -origin(2) : rows - origin(2);
imshow(img, 'XData', xdata, 'YData', ydata)
axis off
hold on
x_plot = pptrials{trialIdx,2}.x.position*angle2img;
y_plot = -pptrials{trialIdx,2}.y.position*angle2img;
color = jet(length(x_plot));
clim = [0, length(x_plot)/Fs];
scatter(x_plot, y_plot, 5, color)
text(double(x_plot(1,1)),double(y_plot(1,1)), '\leftarrow Start', 'color', 'white')
colormap(color)
caxis(clim)
h = colorbar('southoutside');
title(h, 'Time [s]')

%% Depth mapping

% right
rangename = sprintf('rRange%03d.mat', pptrials{trialIdx,1}.ImageNum);
load(fullfile(imgpath, rangename))
figure
[rows, columns] = size(rangeImg);
origin = [columns/2, rows/2];
xdata = -origin(1) : columns - origin(1);
ydata = -origin(2) : rows - origin(2);
imagesc(rangeImg, 'XData', xdata, 'YData', ydata, [0 70]);
axis off
hold on
x_plot = pptrials{trialIdx,1}.x.position*angle2img;
y_plot = -pptrials{trialIdx,1}.y.position*angle2img;
scatter(x_plot, y_plot, 5, 'k')
text(double(x_plot(1)),double(y_plot(1)), '\leftarrow Start', 'color', 'white')
h = colorbar('southoutside');
title(h, 'Depth [m]')
daspect([1 1 1])

% left
rangename = sprintf('lRange%03d.mat', pptrials{trialIdx,1}.ImageNum);
load(fullfile(imgpath, rangename))
figure
[rows, columns] = size(rangeImg);
origin = [columns/2, rows/2];
xdata = -origin(1) : columns - origin(1);
ydata = -origin(2) : rows - origin(2);
imagesc(rangeImg, 'XData', xdata, 'YData', ydata, [0 70]);
axis off
hold on
x_plot = pptrials{trialIdx,2}.x.position*angle2img;
y_plot = -pptrials{trialIdx,2}.y.position*angle2img;
scatter(x_plot, y_plot, 5, 'k')
text(double(x_plot(1)),double(y_plot(1)), '\leftarrow Start', 'color', 'white')
h = colorbar('southoutside');
title(h, 'Depth [m]')
daspect([1 1 1])

%% Vergence comparison to depth

% choose size over which to average
avgradius = 30; %arcmin
pixrad = ceil(avgradius/pixel2angle);

rangename = sprintf('rRange%03d.mat', pptrials{trialIdx,1}.ImageNum);
load(fullfile(imgpath, rangename))

rdepth = zeros(1, length(x_plot));
x_pix = ceil(x_plot+1280/2);
y_pix = ceil(y_plot+720/2);
for i = 1:length(x_plot)
    inrangeY = y_pix(i)-pixrad >= 1 & y_pix(i)+pixrad <= size(rangeImg,1);
    inrangeX = x_pix(i)-pixrad >= 1 & x_pix(i)+pixrad <= size(rangeImg,2);
    if ~(inrangeY && inrangeX)
        continue
    end
    rdepth(i) = mean(rangeImg(y_pix(i)-pixrad:y_pix(i)+pixrad, x_pix(i)-pixrad:x_pix(i)+pixrad), 'all');
end

% Left depth (looked exactly the same, probably due to averaging)
%{
rangename = sprintf('lRange%03d.mat', pptrials{trialIdx,1}.ImageNum);
load(fullfile(imgpath, rangename))

ldepth = zeros(1, length(x_plot));
x_pix = ceil(x_plot+1280/2);
y_pix = ceil(y_plot+720/2);
for i = 1:length(x_plot)
    inrangeY = y_pix(i)-pixrad >= 1 & y_pix(i)+pixrad <= size(rangeImg,1);
    inrangeX = x_pix(i)-pixrad >= 1 & x_pix(i)+pixrad <= size(rangeImg,2);
    if ~(inrangeY && inrangeX)
        continue
    end
    ldepth(i) = mean(rangeImg(y_pix(i)-pixrad:y_pix(i)+pixrad, x_pix(i)-pixrad:x_pix(i)+pixrad), 'all');
end
%}
time = linspace(0, 6, length(rdepth));
figure
hold on
plot(time, rdepth, '-k', 'displayname', 'Depth of scene [m]')
ylim([0 60])
yyaxis right
plot(time, pptrials{trialIdx,1}.vs_components(:, 3), '-b', 'displayname', 'Vergence [arcmin]')
%plot(rdepth, 'displayname', 'Depth of left scene [m]')
xlabel('Time [s]')

% figure
% scatter(pptrials{trialIdx,1}.vs_components(:, 3), rdepth, 5)
% xlabel('Vergence')
% ylabel('depth')

%% Save all graphs for all trials
%{
for i = 1:length(pptrials)
    visualize3dFV(datafile, i, "C:\Users\ryckm_000\Documents\MATLAB\Freeview\figures\3dplots");
end
%}