% Converts EyeRIS data set to MATLAB data
pathtodata = "C:\Users\ryckm_000\Documents\MATLAB\Freeview\Freeview MATLAB\RawDataCopy";

% Desired file name for data
outfile = 'MAC_pptrials.mat';

slec = eis_readData_binocular([], 'x1');
slec = eis_readData_binocular(slec, 'y1');
slec = eis_readData_binocular(slec, 'x2');
slec = eis_readData_binocular(slec, 'y2');

slec = eis_readData_binocular(slec, 'stream', 0, 'int'); % right trigger
slec = eis_readData_binocular(slec, 'stream', 1, 'int'); % left trigger

slec = eis_readData_binocular(slec, 'stream', 2, 'double'); % stabalized eye 1 data x1.y1
slec = eis_readData_binocular(slec, 'stream', 3, 'double');  % stabalized eye 2 data x1.y1

slec = eis_readData_binocular(slec, 'trigger', 'blink1');
slec = eis_readData_binocular(slec, 'trigger', 'notrack1');
slec = eis_readData_binocular(slec, 'trigger', 'blink2');
slec = eis_readData_binocular(slec, 'trigger', 'notrack2');

slec = eis_readData_binocular(slec,'photocell');
slec = eis_readData_binocular(slec,'spf');

slec = eis_readData_binocular(slec, 'uservar','SubjectName');
slec = eis_readData_binocular(slec, 'uservar','DEBUG');
slec = eis_readData_binocular(slec, 'uservar','Trial');

slec = eis_readData_binocular(slec, 'uservar','StabCond');
slec = eis_readData_binocular(slec, 'uservar','EyeCond');

slec = eis_readData_binocular(slec, 'uservar','RecalFrequency');
slec = eis_readData_binocular(slec, 'uservar','RecalIncrement');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetX1');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetY1');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetX2');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetY2');

slec = eis_readData_binocular(slec, 'uservar','PixelAngle');

slec = eis_readData_binocular(slec, 'uservar','ImageResolution');
%slec = eis_readData_binocular(slec, 'uservar','ImageScale'); %use string names
slec = eis_readData_binocular(slec, 'uservar','ImageNum');

warning off
data = readdata(pathtodata, slec, outfile);
%%
pptrials  = preprocessing(data);
save(strcat(pathtodata, '\', outfile), 'data', 'pptrials')
warning on