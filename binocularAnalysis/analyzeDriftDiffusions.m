function out = analyzeDriftDiffusions(pptrials, counts, nboots, params, DOPLOT, imgPath)

subjname = pptrials{1, 1}.SubjectName;

buffer = 50;

fix1 = struct('x', [], 'y', []);
fix2 = struct('x', [], 'y', []);
fix_vers = struct('x', [], 'y', []);
fix_verg = struct('x', [], 'y', []);


fix_info = struct('task', [], 'badcalibration', []);

save_name = fullfile(imgPath, sprintf('%s_diffcon_%i.mat', subjname, nboots));

out = struct();

cnt = 0;
for ii = 1:size(pptrials, 1)
    if counts.exclude(ii)
        continue;
    end
    
    taskstarts = nan(length(params.tasks), 1);
    taskstops = nan(length(params.tasks), 1);
    for ti = 1:length(params.tasks)
        taskstarts(ti) = pptrials{ii, 1}.(params.startTimes{ti});
        taskstops(ti) = pptrials{ii, 1}.(params.stopTimes{ti});
    end
    
    isdrift = pptrials{ii, 1}.isdrift & pptrials{ii, 2}.isdrift;
    
    [ss, ee] = getIndicesFromBin(isdrift);
    
    for jj = 1:length(ss)
        s = ss(jj) + buffer;
        e = ee(jj) - buffer;
        
        if e - s < 0
            continue;
        end
        
        task = find(s >= taskstarts & s <= taskstops) - 1;
        if isempty(task)
            continue; 
        end
        
        cnt = cnt + 1;
        
        fix1(cnt).x = pptrials{ii, 1}.x.shifted(s:e);
        fix1(cnt).y = pptrials{ii, 1}.y.shifted(s:e);
        
        fix2(cnt).x = pptrials{ii, 2}.x.shifted(s:e);
        fix2(cnt).y = pptrials{ii, 2}.y.shifted(s:e);
        
        % should have divided these by 2 - compensated for in plotting and
        % downstream analysis
        fix_vers(cnt).x = pptrials{ii,1}.vs_components(s:e, 1)' / 2;
        fix_vers(cnt).y = pptrials{ii,1}.vs_components(s:e, 2)' / 2;
        
        fix_verg(cnt).x = pptrials{ii,1}.vs_components(s:e, 3)' / 2;
        fix_verg(cnt).y = pptrials{ii,1}.vs_components(s:e, 4)' / 2;
        
        if any(isnan(fix1(cnt).x) | isinf(fix1(cnt).x))
            keyboard;
        end
        
        
        fix_info.task(cnt) = task;
        
        fix_info.badcalibration(cnt) = counts.badcalibration(ii);
    end
end

if ~exist(save_name, 'file')
    %% calculate diffusion constants for each task
    diffCon = nan(length(params.plottasks), 4);
    diffConBoot = nan(length(params.plottasks), 4, nboots);
    Dsq = cell(length(params.plottasks), 4, 2);
    
    for ti = params.plottasks
        useTrials = fix_info.task == ti;
        useTrials = useTrials(:);
        
        uIdx = find(useTrials);
        
        [~, ~, dc_fix_1, ~, Dsq_fix_1, SingleSegmentDsq_fix_1] = ...
            CalculateDiffusionCoef(fix1(useTrials));
        [~, ~, dc_fix_2, ~, Dsq_fix_2, SingleSegmentDsq_fix_2] = ...
            CalculateDiffusionCoef(fix2(useTrials));
        
        useTrials2 = useTrials & ~fix_info.badcalibration(:);
        uIdx2 = find(useTrials2);
        [~, ~, dc_fix_vers, ~, Dsq_fix_vers, SingleSegmentDsq_fix_vers] = ...
            CalculateDiffusionCoef(fix_vers(useTrials2));
        [~, ~, dc_fix_verg, ~, Dsq_fix_verg, SingleSegmentDsq_fix_verg] = ...
            CalculateDiffusionCoef(fix_verg(useTrials2));
        
        diffCon(ti+1, 1) = dc_fix_1;
        diffCon(ti+1, 2) = dc_fix_2;
        diffCon(ti+1, 3) = dc_fix_vers;
        diffCon(ti+1, 4) = dc_fix_verg;
        
        Dsq{ti+1, 1, 1} = Dsq_fix_1;
        Dsq{ti+1, 1, 2} = SingleSegmentDsq_fix_1;
        
        Dsq{ti+1, 2, 1} = Dsq_fix_2;
        Dsq{ti+1, 2, 2} = SingleSegmentDsq_fix_2;
        
        Dsq{ti+1, 3, 1} = Dsq_fix_vers;
        Dsq{ti+1, 3, 2} = SingleSegmentDsq_fix_vers;
        
        Dsq{ti+1, 4, 1} = Dsq_fix_verg;
        Dsq{ti+1, 4, 2} = SingleSegmentDsq_fix_verg;
        ti1 = ti + 1;
        %% do bootstrapping also
        parfor bi = 1:nboots
            rs = randsample(uIdx, length(uIdx), true);
            [~, ~, dc_fix_1] = ...
                CalculateDiffusionCoef(fix1(rs));
            [~, ~, dc_fix_2] = ...
                CalculateDiffusionCoef(fix2(rs));
            
            rs = randsample(uIdx2, length(uIdx), true);
            [~, ~, dc_fix_vers] = ...
                CalculateDiffusionCoef(fix_vers(rs));
            [~, ~, dc_fix_verg] = ...
                CalculateDiffusionCoef(fix_verg(rs));
            
            diffConBoot(ti1, :, bi) = [dc_fix_1; dc_fix_2; dc_fix_vers; dc_fix_verg];
        end
    end
    if nboots >= 100 && ~exist(save_name, 'file')
            save(save_name, 'fix*', 'Dsq*', 'diffCon*');
        end
else
    load(save_name);
end

out.Dsq = Dsq;
out.diffCon = diffCon;
out.diffConBoot = diffConBoot;

if DOPLOT
    plotDriftDiffusions(out, params, subjname, imgPath);
end

end

