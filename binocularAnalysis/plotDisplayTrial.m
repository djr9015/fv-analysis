function plotDisplayTrial(pptrials, trialIdx, fh, saccadePairs, saccadeMon, overshoot, DOSAVEPLOT)
if ~exist('overshoot', 'var') || isempty(overshoot)
    overshoot = false; 
end

if ~overshoot
    x1 = pptrials{trialIdx, 1}.x.shifted;
    y1 = pptrials{trialIdx, 1}.y.shifted;
    x2 = pptrials{trialIdx, 2}.x.shifted;
    y2 = pptrials{trialIdx, 2}.y.shifted;
else
    x1 = pptrials{trialIdx, 1}.x.overshoot;
    y1 = pptrials{trialIdx, 1}.y.overshoot;
    x2 = pptrials{trialIdx, 2}.x.overshoot;
    y2 = pptrials{trialIdx, 2}.y.overshoot;
end

yl = double([min([x1, y1, x2, y2]), max([x1, y1, x2, y2])]);
yl = [-50, 50];

flds = {'notracks', 'blinks', 'invalid'};
cols = {[.5 .5 .5], [.5 .5 .5], [.7 .7 .7]};%, [1, .5, .3], [1, .5, .3]};

nPlots = 3;
ax = nan(nPlots, 1);

%% saccade data constants
run('saccadeMatrixIndex.m');

%% now plot things
figure(fh); clf;

%% plot x, y traces for eye movements
for ei = 1:2
    ax(ei) = subplot(nPlots, 1, ei); hold on;
    
    % no tracks / blinks
    for fi = 1:length(flds)
        fld = flds{fi};
        starts = pptrials{trialIdx, ei}.(fld).start;
        stops = starts + pptrials{trialIdx, ei}.(fld).duration - 1;
        
        for si = 1:length(starts)
            patch([starts(si), starts(si), stops(si), stops(si)],...
                [yl(1), yl(2), yl(2), yl(1)],...
                cols{fi},...
                'EdgeColor', 'none',...
                'FaceAlpha', .3);
        end
    end
    
    if exist('saccadePairs', 'var') && ~isempty(saccadePairs)
        % binocular saccades / microsaccades
        for ii = 1:size(saccadePairs, 1)
            if ei == 1
                start = saccadePairs(ii, EYE1_START);
                stop = saccadePairs(ii, EYE1_STOP);
            else
                start = saccadePairs(ii, EYE2_START);
                stop = saccadePairs(ii, EYE2_STOP);
            end
            
            patch([start, start, stop, stop],...
                [yl(1), yl(2), yl(2), yl(1)],...
                [1, .5, .3],...
                'EdgeColor', 'none',...
                'FaceAlpha', .3);
            text((start+stop)/2, yl(1)+.9*diff(yl), sprintf('S%i', ii));
        end
        
        % potential monocular saccades
        for ii = 1:size(saccadeMon, 1)
            if ei == 1
                start = saccadeMon(ii, EYE1_START);
                stop = saccadeMon(ii, EYE1_STOP);
                
                % check eye 2
                if isnan(saccadeMon(ii, EYE2_START)) % monocular
                    col = 'g';
                else
                    col = [1, .5, .3];
                end
            else
                start = saccadeMon(ii, EYE2_START);
                stop = saccadeMon(ii, EYE2_STOP);
                
                % check eye 1
                if isnan(saccadeMon(ii, EYE1_START)) % monocular
                    col = 'g';
                else
                    col = [1, .5, .3];
                end
            end
            patch([start, start, stop, stop],...
                [yl(1), yl(2), yl(2), yl(1)],...
                col,...
                'EdgeColor', 'none',...
                'FaceAlpha', .3);
            text((start+stop)/2, yl(1)+.9*diff(yl), sprintf('M%i', ii));
        end
    end
    
    % response times
    for rr = 1:6
        tmp = double(pptrials{trialIdx, 1}.(sprintf('ResponseTimeList%i', rr)));
        vertLineThrough(tmp + pptrials{trialIdx, 1}.timeRampStart, 'k');
    end
    
    vertLineThrough(pptrials{trialIdx, ei}.analysisStart, 'k', gca, ':');
    
    if ei == 1
        hx = plot(x1, 'b', 'DisplayName', 'right-x');
        hy = plot(y1, 'r', 'DisplayName', 'right-y');
        
    else
        hx = plot(x2, 'b', 'DisplayName', 'left-x');
        hy = plot(y2, 'r', 'DisplayName', 'left-y');
    end
    legend([hx, hy], 'Location', 'southeast');
    ylim(yl);
    xlim([1, length(x1)]);
    ylabel('(arcmin)');
end

%% plot velocity traces of both eyes
ax(nPlots) = subplot(nPlots, 1, nPlots); hold on;
h1 = plot(pptrials{trialIdx, 1}.velocity / 60, 'g', 'DisplayName', 'right');
h2 = plot(pptrials{trialIdx, 2}.velocity / 60, 'm', 'DisplayName', 'left');
ylabel('velocity (deg / s)');
xlabel('time (ms)');
legend([h1, h2], 'Location', 'northeast');
xlim([1, length(x1)]);
ylim([0, 6]);
lineThrough(3, 'k', gca, ':');

%% link axes
linkaxes(ax, 'x');
linkaxes(ax(1:2), 'y');

%{
for a = 1:length(ax)
    axes(ax(a));
    if ~plotFixation
        tRampStart = pptrials{trialIdx, 1}.timeRampStart;
        patch([0, 0, tRampStart, tRampStart],...
            [yl(1), yl(2), yl(2), yl(1)],...
            [.6 .6 .6], 'EdgeColor', 'none',...
            'FaceAlpha', .5);
    end
    if ~plotTask
        tRampStart = pptrials{trialIdx, 1}.timeRampStart;
        patch([tRampStart, tRampStart, length(x1), length(x1)],...
            [yl(1), yl(2), yl(2), yl(1)],...
            [.6 .6 .6], 'EdgeColor', 'none',...
            'FaceAlpha', .5);
    end
end
%}

end