function [drifts] = driftCharsVS(pptrials, params)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
drifts = struct('trialIdx', [], 'driftIdx', []);


% specify parameters to analyze drifts
smoothing = 31; % smoothing window size (indices)
cutseg = floor(smoothing/2);
maxSpeed = 5 * 60;

%drift_pertrial = length(pptrials{1}.drifts.start);

di_loop = 0;
% loop over all trials
for trialIdx = 1:length(pptrials)
    if ~isfield(pptrials{trialIdx,1},'isdrift')
        continue
    end
    isdrift = pptrials{trialIdx, 1}.isdrift & pptrials{trialIdx, 2}.isdrift;
    tt = isdrift(round(pptrials{trialIdx, 1}.(params.startTimes{1})):round(pptrials{trialIdx, 1}.(params.stopTimes{1})));
    vstt = [pptrials{trialIdx,1}.vs_components(:,1), pptrials{trialIdx,1}.vs_components(:,2), pptrials{trialIdx,1}.vs_components(:,3), pptrials{trialIdx,1}.vs_components(:,4)];

    if any(~tt) && tt(end)
        ee = find(~tt, 1, 'last');
        tt = tt(1:ee);
        vstt = vstt(1:ee,:);
    end

    if any(~tt) && tt(1)
        ss = find(~tt, 1, 'first');
        tt = tt(ss:end);
        vstt = vstt(ss:end,:);
    end
    
    drift_start = [];
    for i = 2:length(tt)
        if tt(i) == 1 && tt(i-1) == 0
            drift_start = [drift_start, i];
        end
    end
    drift_stop = [];
    for i = 2:length(tt)
        if tt(i) == 0 && tt(i-1) == 1
            drift_stop = [drift_stop, i];
        end
    end
            
    % loop through each drift, save its trace to 'drifts'

    drift_pertrial = length(drift_start);
    for di = 1:drift_pertrial
        % start and stop times of this drift segment, extract x and y traces
        startIdx = drift_start(di);
        stopIdx = drift_stop(di);

        versx = vstt(startIdx:stopIdx,1);
        versy = vstt(startIdx:stopIdx,2);
        vergx = vstt(startIdx:stopIdx,3);
        vergy = vstt(startIdx:stopIdx,4);

        if length(versx) < smoothing
            continue; 
        end
        
        %drift duration
        driftduration = (stopIdx-startIdx)/1000; %1000 is sample rate

        % use getDriftChar to measure drift characteristcs
        [span, mn_speed, mn_cur, varx, vary, smx, smy] = ...
            getDriftChar(versx, versy, smoothing, cutseg, maxSpeed);

        % save data to drifts structure
        di_loop = di_loop+1;
        drifts(di_loop).trialIdx = trialIdx;
        drifts(di_loop).driftIdx = di_loop;
        drifts(di_loop).x = smx;
        drifts(di_loop).y = smy;
        drifts(di_loop).vergx = vergx;
        drifts(di_loop).vergy = vergy;
        drifts(di_loop).span = span;
        drifts(di_loop).speed = mn_speed;
        drifts(di_loop).start = drift_start;
        drifts(di_loop).stop = drift_stop;
        drifts(di_loop).duration = driftduration;
        drifts(di_loop).curv = mn_cur;
        drifts(di_loop).varx = varx;
        drifts(di_loop).vary = vary;
        drifts(di_loop).mean_vergence = mean(vergx);
    end
end
end
