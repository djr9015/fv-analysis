function [fix_vers, fix_verg, fix_label] = ...
    splitTrialBinocularDrifts(pptrials, trialIndex)

fixEnd = pptrials{trialIndex, 1}.timeRampStart;
as = (pptrials{trialIndex, 1}.analysisStart ...
    + pptrials{trialIndex, 2}.analysisStart)/2;

x1 = pptrials{trialIndex, 1}.x.shifted;
y1 = pptrials{trialIndex, 1}.y.shifted;

x2 = pptrials{trialIndex, 2}.x.shifted;
y2 = pptrials{trialIndex, 2}.y.shifted;

sigma_H = (x1 + x2)/2;
sigma_V = (y1 + y2)/2;

nu_H = (x1 - x2);
nu_V = (y1 - y2);

isdrift = pptrials{trialIndex, 1}.isdrift...
    & pptrials{trialIndex, 2}.isdrift;

[startInd, stopInd] = getIndicesFromBin(isdrift);

fix_vers(length(startInd)) = struct('x', [], 'y', []);
fix_verg(length(startInd)) = struct('x', [], 'y', []);
fix_label = nan(size(startInd));

for ii = 1:length(startInd)
    ss = startInd(ii);
    ee = stopInd(ii);
    
    fix_vers(ii).x = sigma_H(ss:ee);
    fix_vers(ii).y = sigma_V(ss:ee);
    
    fix_verg(ii).x = nu_H(ss:ee);
    fix_verg(ii).y = nu_V(ss:ee);
    
    if ss < fixEnd
        fix_label(ii) = 0;
    elseif ss > as
        fix_label(ii) = 1;
    else
        fix_label(ii) = 2;
    end
end
end