function [driftBins, vergE] = binDriftsByVergence(drifts, nBins)
% binDriftsByVergence separates a drifts struct (w/ field 'mean_vergence')
% into different elements in the driftBins cell, with an equal number of
% drifts in each bin, in scending order of vergence.
% Outputs the driftBins cell & the vergence levels at each bin edge (vergE)

%convert to cell array
driftfields = fieldnames(drifts);
driftcell = struct2cell(drifts);
sz = size(driftcell);
driftcell = reshape(driftcell, sz(1), [])';
%sort by vergence
verg__field_idx = find((string(driftfields)=='mean_vergence'));
driftcell = sortrows(driftcell, verg__field_idx);
%convert back to struct
driftcell = reshape(driftcell', sz);
driftsSort = cell2struct(driftcell, driftfields, 1);
driftsSort = driftsSort(isnan([driftsSort.mean_vergence]) == 0); %removes nan vergence rows

% bin edges
driftsPerBin = floor(length(driftsSort)/nBins);
E = ones(1, nBins+1);
for i = 1:nBins
    E(i+1) = i*driftsPerBin;
end
E(end) = length(driftsSort);
vergE = [driftsSort(E).mean_vergence];
driftBins = cell(nBins,1);
for i = 1:nBins
    driftBins{i} = driftsSort(E(i):E(i+1));
end
end

