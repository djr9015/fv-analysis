function plotDiffusionConstants(subject, fix1, fix2, fix_vers, fix_verg, fix_label1, fix_label2, fix_label_v, DOPLOT, imgPath)
NBOOTS = 10;

fh = 400;

tasks = {'Fixation', 'Snellen'};

% remove drifts that are too short
[fix1, fix_label1] = removeShortDrifts(fix1, fix_label1);
[fix2, fix_label2] = removeShortDrifts(fix2, fix_label2);

[fix_vers, fix_label_v] = removeShortDrifts(fix_vers, fix_label_v);
fix_verg = removeShortDrifts(fix_verg);


warning('off');
%% monocular diffusion coefficients
use_fix_1 = find(fix_label1 == 0);
use_fix_2 = find(fix_label2 == 0);
use_task_1 = find(fix_label1 == 1);
use_task_2 = find(fix_label2 == 1);

% eye 1
[~, ~, dc_fix_1, ~, Dsq_fix_1, SingleSegmentDsq_fix_1] = ...
    CalculateDiffusionCoef(fix1(use_fix_1));
[~, ~, dc_task_1, ~, Dsq_task_1, SingleSegmentDsq_task_1] = ...
    CalculateDiffusionCoef(fix1(use_task_1));

% eye 2
[~, ~, dc_fix_2, ~, Dsq_fix_2, SingleSegmentDsq_fix_2] = ...
    CalculateDiffusionCoef(fix2(use_fix_2));
[~, ~, dc_task_2, ~, Dsq_task_2, SingleSegmentDsq_task_2] = ...
    CalculateDiffusionCoef(fix2(use_task_2));

dc_fix_1_boot = nan(NBOOTS, 1);
dc_fix_2_boot = nan(NBOOTS, 1);
dc_task_1_boot = nan(NBOOTS, 1);
dc_task_2_boot = nan(NBOOTS, 1);
parfor nb = 1:NBOOTS
    fprintf('%s: %i of %i iterations\n', datestr(now), nb, NBOOTS);
    
    rs = randsample(use_fix_1, length(use_fix_1), true);
    [~, ~, dc] = CalculateDiffusionCoef(fix1(rs));
    dc_fix_1_boot(nb) = dc;
    
    rs = randsample(use_fix_2, length(use_fix_2), true);
    [~, ~, dc] = CalculateDiffusionCoef(fix2(rs));
    dc_fix_2_boot(nb) = dc;
    
    rs = randsample(use_task_1, length(use_task_1), true);
    [~, ~, dc] = CalculateDiffusionCoef(fix1(rs));
    dc_task_1_boot(nb) = dc;
    
    rs = randsample(use_task_2, length(use_task_2), true);
    [~, ~, dc] = CalculateDiffusionCoef(fix2(rs));
    dc_task_2_boot(nb) = dc;
end

%% plot Dsq (monocular)
fh = fh + 1;
figure(fh); clf;
subplot(2, 2, 1); hold on;
plot(1:255, SingleSegmentDsq_fix_1);
plot(1:255, Dsq_fix_1, 'k-', 'linewidth', 2);
xlabel('time lag (ms)');
ylabel('r^2');
title([tasks{1}, ': right eye']);
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim([1, 255]); ylim([0, 60]);
text(25, 50, ...
    sprintf('D = %1.1f (%1.1f\\pm%1.1f)', dc_fix_1, mean(dc_fix_1_boot), std(dc_fix_1_boot)), ...
    'FontSize', 16, 'FontName', 'alpha');

subplot(2, 2, 2); hold on;
plot(1:255, SingleSegmentDsq_fix_2);
plot(1:255, Dsq_fix_2, 'k-', 'linewidth', 2);
xlabel('time lag (ms)');
ylabel('r^2');
title([tasks{1}, ': left eye']);
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim([1, 255]); ylim([0, 60]);
text(25, 50, ...
    sprintf('D = %1.1f (%1.1f\\pm%1.1f)', dc_fix_2, mean(dc_fix_2_boot), std(dc_fix_2_boot)), ...
    'FontSize', 16, 'FontName', 'alpha');

subplot(2, 2, 3); hold on;
plot(1:255, SingleSegmentDsq_task_1);
plot(1:255, Dsq_task_1, 'k-', 'linewidth', 2);
xlabel('time lag (ms)');
ylabel('r^2');
title([tasks{2}, ': right eye']);
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim([1, 255]); ylim([0, 60]);
text(25, 50, ...
    sprintf('D = %1.1f (%1.1f\\pm%1.1f)', dc_task_1, mean(dc_task_1_boot), std(dc_task_1_boot)), ...
    'FontSize', 16, 'FontName', 'alpha');

subplot(2, 2, 4); hold on;
plot(1:255, SingleSegmentDsq_task_2);
plot(1:255, Dsq_task_2, 'k-', 'linewidth', 2);
xlabel('time lag (ms)');
ylabel('r^2');
title([tasks{2}, ': left eye']);
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim([1, 255]); ylim([0, 60]);
text(25, 50, ...
    sprintf('D = %1.1f (%1.1f\\pm%1.1f)', dc_task_2, mean(dc_task_2_boot), std(dc_task_2_boot)), ...
    'FontSize', 16, 'FontName', 'alpha');

annotation('textbox',[.02, .9, .1, .1], 'String', [subject, ': Diffusion Constants'],...
    'FitBoxToText', 'on', 'FontSize', 20, 'FontName', 'arial',...
    'FontWeight', 'bold');

if DOPLOT && ~isempty(imgPath)
    fname = fullfile(imgPath, sprintf('%s_diffCon_monocular_dsq', subject));
    print([fname, '.png'], '-dpng');
    print([fname, '.eps'], '-depsc');
end

%% binocular diffusion coefficients
use_fix_v = find(fix_label_v == 0);
use_task_v = find(fix_label_v == 1);

% horizontal vergence only
fix_verg_H = fix_verg;
for i = 1:length(fix_verg_H)
    fix_verg_H(i).y = zeros(size(fix_verg_H(i).y)); 
end

% version
[~, ~, dc_fix_s, ~, Dsq_fix_s, SingleSegmentDsq_fix_s] = ...
    CalculateDiffusionCoef(fix_vers(use_fix_v));
[~, ~, dc_task_s, ~, Dsq_task_s, SingleSegmentDsq_task_s] = ...
    CalculateDiffusionCoef(fix_vers(use_task_v));

% vergence
[~, ~, dc_fix_g, ~, Dsq_fix_g, SingleSegmentDsq_fix_g] = ...
    CalculateDiffusionCoef(fix_verg(use_fix_v));
[~, ~, dc_task_g, ~, Dsq_task_g, SingleSegmentDsq_task_g] = ...
    CalculateDiffusionCoef(fix_verg(use_task_v));

% vergence (horizontal)
[~, ~, dc_fix_gh, ~, Dsq_fix_gh, SingleSegmentDsq_fix_gh] = ...
    CalculateDiffusionCoef(fix_verg_H(use_fix_v));
[~, ~, dc_task_gh, ~, Dsq_task_gh, SingleSegmentDsq_task_gh] = ...
    CalculateDiffusionCoef(fix_verg_H(use_task_v));

dc_fix_s_boot = nan(NBOOTS, 1);
dc_fix_g_boot = nan(NBOOTS, 1);
dc_fix_gh_boot = nan(NBOOTS, 1);
dc_task_s_boot = nan(NBOOTS, 1);
dc_task_g_boot = nan(NBOOTS, 1);
dc_task_gh_boot = nan(NBOOTS, 1);
parfor nb = 1:NBOOTS
    fprintf('%s: %i of %i iterations\n', datestr(now), nb, NBOOTS);
    
    rs = randsample(use_fix_v, length(use_fix_v), true);
    [~, ~, dc] = CalculateDiffusionCoef(fix_vers(rs));
    dc_fix_s_boot(nb) = dc;
    
    rs = randsample(use_fix_v, length(use_fix_v), true);
    [~, ~, dc] = CalculateDiffusionCoef(fix_verg(rs));
    dc_fix_g_boot(nb) = dc;
    
    rs = randsample(use_fix_v, length(use_fix_v), true);
    [~, ~, dc] = CalculateDiffusionCoef(fix_verg_H(rs));
    dc_fix_gh_boot(nb) = 2*dc;
    
    rs = randsample(use_task_v, length(use_task_v), true);
    [~, ~, dc] = CalculateDiffusionCoef(fix_vers(rs));
    dc_task_s_boot(nb) = dc;
    
    rs = randsample(use_task_v, length(use_task_v), true);
    [~, ~, dc] = CalculateDiffusionCoef(fix_verg(rs));
    dc_task_g_boot(nb) = dc;
    
    rs = randsample(use_task_v, length(use_task_v), true);
    [~, ~, dc] = CalculateDiffusionCoef(fix_verg_H(rs));
    dc_task_gh_boot(nb) = 2*dc;
end

%% plot Dsq (binocular)
fh = fh + 1;
figure(fh); clf;
subplot(2, 3, 1); hold on;
plot(1:255, SingleSegmentDsq_fix_s);
plot(1:255, Dsq_fix_s, 'k-', 'linewidth', 2);
xlabel('time lag (ms)');
ylabel('r^2');
title([tasks{1}, ': Version']);
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim([1, 255]); ylim([0, 60]);
text(25, 50, ...
    sprintf('D = %1.1f (%1.1f\\pm%1.1f)', dc_fix_s, mean(dc_fix_s_boot), std(dc_fix_s_boot)), ...
    'FontSize', 16, 'FontName', 'alpha');

subplot(2, 3, 2); hold on;
plot(1:255, SingleSegmentDsq_fix_g);
plot(1:255, Dsq_fix_g, 'k-', 'linewidth', 2);
xlabel('time lag (ms)');
ylabel('r^2');
title([tasks{1}, ': Vergence']);
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim([1, 255]); ylim([0, 60]);
text(25, 50, ...
    sprintf('D = %1.1f (%1.1f\\pm%1.1f)', dc_fix_g, mean(dc_fix_g_boot), std(dc_fix_g_boot)), ...
    'FontSize', 16, 'FontName', 'alpha');

subplot(2, 3, 3); hold on;
plot(1:255, SingleSegmentDsq_fix_gh);
plot(1:255, Dsq_fix_gh, 'k-', 'linewidth', 2);
xlabel('time lag (ms)');
ylabel('r^2');
title([tasks{1}, ': Vergence (Horizontal)']);
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim([1, 255]); ylim([0, 60]);
text(25, 50, ...
    sprintf('D = %1.1f (%1.1f\\pm%1.1f)', dc_fix_gh, mean(dc_fix_gh_boot), std(dc_fix_gh_boot)), ...
    'FontSize', 16, 'FontName', 'alpha');

subplot(2, 3, 4); hold on;
plot(1:255, SingleSegmentDsq_task_s);
plot(1:255, Dsq_task_s, 'k-', 'linewidth', 2);
xlabel('time lag (ms)');
ylabel('r^2');
title([tasks{2}, ': Version']);
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim([1, 255]); ylim([0, 60]);
text(25, 50, ...
    sprintf('D = %1.1f (%1.1f\\pm%1.1f)', dc_task_s, mean(dc_task_s_boot), std(dc_task_s_boot)), ...
    'FontSize', 16, 'FontName', 'alpha');

subplot(2, 3, 5); hold on;
plot(1:255, SingleSegmentDsq_task_g);
plot(1:255, Dsq_task_g, 'k-', 'linewidth', 2);
xlabel('time lag (ms)');
ylabel('r^2');
title([tasks{2}, ': Vergence']);
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim([1, 255]); ylim([0, 60]);
text(25, 50, ...
    sprintf('D = %1.1f (%1.1f\\pm%1.1f)', dc_task_g, mean(dc_task_g_boot), std(dc_task_g_boot)), ...
    'FontSize', 16, 'FontName', 'alpha');

subplot(2, 3, 6); hold on;
plot(1:255, SingleSegmentDsq_task_gh);
plot(1:255, Dsq_task_gh, 'k-', 'linewidth', 2);
xlabel('time lag (ms)');
ylabel('r^2');
title([tasks{2}, ': Vergence (Horizontal)']);
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim([1, 255]); ylim([0, 60]);
text(25, 50, ...
    sprintf('D = %1.1f (%1.1f\\pm%1.1f)', dc_task_gh, mean(dc_task_gh_boot), std(dc_task_gh_boot)), ...
    'FontSize', 16, 'FontName', 'alpha');

annotation('textbox',[.02, .9, .1, .1], 'String', [subject, ': Diffusion Constants'],...
    'FitBoxToText', 'on', 'FontSize', 20, 'FontName', 'arial',...
    'FontWeight', 'bold');

if DOPLOT && ~isempty(imgPath)
    fname = fullfile(imgPath, sprintf('%s_diffCon_binocular_dsq', subject));
    print([fname, '.png'], '-dpng');
    print([fname, '.eps'], '-depsc');
end

end

function [fix, fix_label] = removeShortDrifts(fix, fix_label, minlength)
if ~exist('minlength', 'var')
    minlength = 50; 
end
if ~exist('fix_label', 'var')
    fix_label = nan(size(fix)); 
end

kill = false(size(fix));
for i = 1:length(fix)
    if length(fix(i).x) < minlength
        kill(i) = true; 
    end
end
fix(kill) = [];
fix_label(kill) = [];
end