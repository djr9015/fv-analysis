function plotFixationInstability(pptrials, counts, whichRegion, DOPLOT, imgPath)
% heatmaps of gaze instability during fixation only - convert horizontal
% and vertical traces into version and vergence components to plot in 3d.
% Assume that the vertical vergence is always 0

if ~exist('imgPath', 'var')
    imgPath = []; 
end

switch whichRegion
    case 'Fixation'
        fh = 50;
    case 'Snellen'
        fh = 60;
end

subjname = pptrials{1, 1}.Subject_Name;

nTrials = size(pptrials, 1);

all_sigma_H = cell(nTrials, 1);
all_sigma_V = cell(nTrials, 1);
all_nu_H = cell(nTrials, 1);
all_nu_V = cell(nTrials, 1);
all_bgp = cell(nTrials, 1);
for ii = 1:length(pptrials)
    if counts.exclude(ii) || counts.badcalibration(ii)
        continue; 
    end
    
    switch whichRegion
        case 'Fixation'
            ss = 300;
            ee = round(pptrials{ii, 1}.timeRampStart);
        case 'Snellen'
            ss = round(pptrials{ii, 1}.analysisStart);
            ee = pptrials{ii, 1}.samples;
    end
    
    sigma_H = pptrials{ii, 1}.vs_components(ss:ee, 1)' / 2;
    sigma_V = pptrials{ii, 1}.vs_components(ss:ee, 2)' / 2;
    
    nu_H = pptrials{ii, 1}.vs_components(ss:ee, 3)' / 2;
    nu_V = pptrials{ii, 1}.vs_components(ss:ee, 4)' / 2;
    
    bgp = double(pptrials{ii}.r_bgp(:, ss:ee));
    
    all_sigma_H{ii} = sigma_H;
    all_sigma_V{ii} = sigma_V;
    all_nu_H{ii} = nu_H;
    all_nu_V{ii} = nu_V;
    all_bgp{ii} = bgp;
end
sigma_H = cat(2, all_sigma_H{:});
sigma_V = cat(2, all_sigma_V{:});
nu_H = cat(2, all_nu_H{:});
nu_V = cat(2, all_nu_V{:});
bgp = cat(2, all_bgp{:});

kill = isnan(sigma_H);
sigma_H(kill) = [];
sigma_V(kill) = [];
nu_H(kill) = [];
nu_V(kill) = [];
bgp(:, kill) = [];

%% plot things

%% version: Hor v Vert heatmap
xedges = linspace(-35, 35, 120);
yedges = linspace(-20, 20, 60);
nhv = histcounts2(sigma_V, sigma_H, yedges, xedges);
figure(fh); clf; 
imagesc(xedges, yedges, nhv);
colormap(flipud(colormap('hot')));
axis image;
xlabel('H: version (arcmin)', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
ylabel('V: version (arcmin)', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
title(sprintf('distribution of gaze position (%s)', whichRegion), 'FontSize', 16, 'FontName', 'arial');
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim(xedges([1, end]));
ylim(yedges([1, end]));
text(xedges(1)+.05 * range(xedges), yedges(end)-.1 * range(yedges),...
    sprintf('H = %1.2f \\pm %1.2f', nanmean(sigma_H), std(sigma_H)),...
    'FontSize', 16, 'FontName', 'arial');
text(xedges(1)+.05 * range(xedges), yedges(end)-.2 * range(yedges),...
    sprintf('V = %1.2f \\pm %1.2f', nanmean(sigma_V), std(sigma_V)),...
    'FontSize', 16, 'FontName', 'arial');
set(gca, 'YDir', 'normal');

annotation('textbox',[.02, .9, .1, .1], 'String', subjname,...
        'FitBoxToText', 'on', 'FontSize', 20, 'FontName', 'arial',...
        'FontWeight', 'bold');

if DOPLOT && ~isempty(imgPath)
    fname = fullfile(imgPath, sprintf('%s_gazeDist_HV', whichRegion));
    print([fname, '.png'], '-dpng');
    print([fname, '.eps'], '-depsc');
end

%% Hor Vergence vs. version
fh = fh + 1;
nhd = histcounts2(nu_H, sigma_H, yedges, xedges);
figure(fh); clf; 
imagesc(xedges, yedges, nhd);
colormap(flipud(colormap('hot')));
axis image;
xlabel('H: version (arcmin)', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
ylabel('H:vergence (arcmin)', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
title(sprintf('distribution of gaze position (%s)', whichRegion), 'FontSize', 16, 'FontName', 'arial');
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim(xedges([1, end]));
ylim(yedges([1, end]));
set(gca, 'YDir', 'normal');
text(xedges(1)+.05 * range(xedges), yedges(end)-.1 * range(yedges),...
    sprintf('version = %1.2f \\pm %1.2f', mean(sigma_H), std(sigma_H)), ...
    'FontSize', 16, 'FontName', 'arial');
text(xedges(1)+.05 * range(xedges), yedges(end)-.2 * range(yedges),...
    sprintf('vergence = %1.2f \\pm %1.2f', mean(nu_H), std(nu_H)),...
    'FontSize', 16, 'FontName', 'arial');

annotation('textbox',[.02, .9, .1, .1], 'String', subjname,...
        'FitBoxToText', 'on', 'FontSize', 20, 'FontName', 'arial',...
        'FontWeight', 'bold');

if DOPLOT && ~isempty(imgPath)
    fname = fullfile(imgPath, sprintf('%s_gazeDist_HD', whichRegion));
    print([fname, '.png'], '-dpng');
    print([fname, '.eps'], '-depsc');
end

%% Vergence Components
fh = fh + 1;
nhd = histcounts2(nu_V, nu_H, yedges, xedges);
figure(fh); clf; 
imagesc(xedges, yedges, nhd);
colormap(flipud(colormap('hot')));
axis image;
xlabel('H: vergence (arcmin)', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
ylabel('V: vergence (arcmin)', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
title(sprintf('distribution of gaze position (%s)', whichRegion), 'FontSize', 16, 'FontName', 'arial');
set(gca, 'FontSize', 16, 'FontName', 'arial');
xlim(xedges([1, end]));
ylim(yedges([1, end]));
text(xedges(1)+.05 * range(xedges), yedges(end)-.1 * range(yedges),...
    sprintf('H = %1.2f \\pm %1.2f', mean(nu_H), std(nu_H)), ...
    'FontSize', 16, 'FontName', 'arial');
text(xedges(1)+.05 * range(xedges), yedges(end)-.2 * range(yedges),...
    sprintf('V = %1.2f \\pm %1.2f', mean(nu_V), std(nu_V)),...
    'FontSize', 16, 'FontName', 'arial');
set(gca, 'YDir', 'normal');
annotation('textbox',[.02, .9, .1, .1], 'String', subjname,...
        'FitBoxToText', 'on', 'FontSize', 20, 'FontName', 'arial',...
        'FontWeight', 'bold');

if DOPLOT && ~isempty(imgPath)
    fname = fullfile(imgPath, sprintf('%s_gazeDist_vergence', whichRegion));
    print([fname, '.png'], '-dpng');
    print([fname, '.eps'], '-depsc');
end

% 3d isosurface plot
n = histcounts3(nu_H, sigma_H, sigma_V, {yedges, xedges, yedges}); 
n = n / sum(n(:));

[X2, Y2, Z2] = meshgrid(xedges, yedges, yedges);

q = 1-[.9 .5 .1]; % had standard deviations but it didn't look nice
t = [.05, .3, 1];
colfrac = [.2, .5, .99];

tmp = sort(n(:));
tmp(tmp == 0) = [];
tmpindex = interp1(cumsum(tmp), 1:length(tmp), q, 'nearest');
qv = tmp(tmpindex);

fh = fh + 1;
figure(fh); clf; hold on;

cols = flipud(colormap('hot'));
cols = cols(:, [3, 2, 1]);
cols = cols(1:end-10, :);
for qi = 1:length(q)
    p = patch(isosurface(X2, Y2, Z2, n, qv(qi)));
    isonormals(X2, Y2, Z2, n, p);
    p.FaceAlpha = t(qi);
    
    p.FaceColor = cols(round(colfrac(qi)*size(cols, 1)), :);
    p.EdgeColor = 'none';
end
daspect([1 1 1])
view(3); 
axis tight;
camlight ;
lighting gouraud;

switch whichRegion
    case 'Snellen'
        xlim(yedges([1, end]));
        ylim(xedges([1, end]));
        zlim(yedges([1, end]));
    case 'Fixation'
        xlim(yedges([1, end]));
        ylim([-20, 20]);
        zlim(yedges([1, end]));
end

ylabel('$x$', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
zlabel('$y$', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
xlabel('$V_x$', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
grid on;

title(sprintf('distribution of gaze position (%s)', whichRegion));
set(gca, 'FontSize', 16, 'FontName', 'arial');

if DOPLOT && ~isempty(imgPath)
    fname = fullfile(imgPath, sprintf('%s_gazeDist_HVD', whichRegion));
    print([fname, '.png'], '-dpng');
    print([fname, '.eps'], '-depsc');
    saveas(fh, [fname, '.fig'], 'fig');
end


%% 3d isosurface plot of binocular gaze point
xedges = linspace(quantile(bgp(1, :), .1), quantile(bgp(1, :), .9), 30);
yedges = linspace(quantile(bgp(2, :), .01), quantile(bgp(2, :), .99), 60);
zedges = linspace(quantile(bgp(3, :), .01), quantile(bgp(3, :), .99), 30);
[n] = histcounts3(bgp(1, :), bgp(2, :), bgp(3, :),...
    {yedges, xedges, zedges}); % why did I make this so confusing?
n = n / sum(n(:));

[X2, Y2, Z2] = meshgrid(xedges, yedges, zedges);

q = [.1, .5, .9]; % had standard deviations but it didn't look nice
t = [.05, .3, 1];
colfrac = [.2, .5, .99];

tmp = sort(n(:));
tmp(tmp == 0) = [];
tmpindex = interp1(cumsum(tmp), 1:length(tmp), q, 'nearest');
qv = tmp(tmpindex);

fh = fh + 1;
figure(fh); clf; hold on;

cols = flipud(colormap('hot'));
cols = cols(:, [3, 2, 1]);
cols = cols(1:end-10, :);
for qi = 1:length(q)
    p = patch(isosurface(X2, Y2, Z2, n, qv(qi)));
    isonormals(X2, Y2, Z2, n, p);
    p.FaceAlpha = t(qi);
    
    p.FaceColor = cols(round(colfrac(qi)*size(cols, 1)), :);
    p.EdgeColor = 'none';
end
daspect([.5 1 1])
view(3); 
axis tight;
camlight ;
lighting gouraud;

switch whichRegion
    case 'Snellen'
        xlim(xedges([1, end]));
        ylim(yedges([1, end]));
        zlim(zedges([1, end]));
    case 'Fixation'
        xlim(xedges([1, end]));
        ylim(yedges([1, end]));
        zlim(zedges([1, end]));
end

ylabel('horizontal (mm)', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
zlabel('vertical (mm)', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
xlabel('distance (mm)', 'FontSize', 16, 'FontName', 'arial', 'interpreter', 'latex');
grid on;

 set(gca, 'View', [-86.7200    2.3200]);
 set(gca, 'YDir', 'reverse');

title(sprintf('distribution of gaze position (%s)', whichRegion));
set(gca, 'FontSize', 16, 'FontName', 'arial');

if DOPLOT && ~isempty(imgPath)
    fname = fullfile(imgPath, sprintf('%s_gazeDist_bgp', whichRegion));
    print([fname, '.png'], '-dpng');
    print([fname, '.eps'], '-depsc');
    saveas(fh, [fname, '.fig'], 'fig');
end

end