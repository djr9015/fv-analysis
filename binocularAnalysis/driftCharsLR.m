function [driftsLR] = driftCharsLR(pptrials)
% creates 1x2 drift struct for left/right eye analysis
drifts1 = struct('x', [], 'y', [], 'trialIdx', [], 'driftIdx', []);
drifts2 = struct('x', [], 'y', [], 'trialIdx', [], 'driftIdx', []);
driftsLR = {drifts1, drifts2};

num_trials = length(pptrials);
Fs = 1000; %sample rate

% specify parameters to analyze drifts
smoothing = 31; % smoothing window size (indices)
cutseg = floor(smoothing/2);
maxSpeed = 5 * 60;

drift_pertrial = length(pptrials{1}.drifts.start);
% loop for R/L
for side = 1:2
    di_loop = 0;
    % loop over all trials
    for trialIdx = 1:num_trials
        drift_pertrial = length(pptrials{trialIdx,side}.drifts.start);
        % loop through each drift, save its trace to 'drifts'
        for di = 1:drift_pertrial
            % start and stop times of this drift segment, extract x and y traces
            startIdx = pptrials{trialIdx,side}.drifts.start(di);
            stopIdx = startIdx + pptrials{trialIdx,side}.drifts.duration(di) - 1;

            xdrift = pptrials{trialIdx,side}.x.position(startIdx:stopIdx);
            ydrift = pptrials{trialIdx,side}.y.position(startIdx:stopIdx);
            
            %find mean vergence for given drift (x component only)
            verg_avg = mean(pptrials{trialIdx,1}.vs_components(startIdx:stopIdx, 3));
            
            %drift duration
            driftduration = (stopIdx-startIdx)/Fs;

            if length(xdrift) < smoothing
                continue; 
            end

            % use getDriftChar to measure drift characteristcs
            [span, mn_speed, mn_cur, varx, vary, smx, smy] = ...
                getDriftChar(xdrift, ydrift, smoothing, cutseg, maxSpeed);

            % save data to drifts structure
            di_loop = di_loop + 1; %index of drift including all previous trials
            driftsLR{side}(di_loop).x = smx;
            driftsLR{side}(di_loop).y = smy;
            driftsLR{side}(di_loop).trialIdx = trialIdx;
            driftsLR{side}(di_loop).driftIdx = di_loop;
            driftsLR{side}(di_loop).span = span;
            driftsLR{side}(di_loop).speed = mn_speed;
            driftsLR{side}(di_loop).duration = driftduration;
            driftsLR{side}(di_loop).curv = mn_cur;
            driftsLR{side}(di_loop).varx = varx;
            driftsLR{side}(di_loop).vary = vary;
            driftsLR{side}(di_loop).mean_vergence = verg_avg;
%             drifts{side}(di_loop).count = driftCt;
            
%             if ~isempty(drifts{side}(di_loop).driftIdx)
%                 driftCt = driftCt+1;
%             end
        end
    end
end
end

