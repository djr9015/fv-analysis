function pptrials = updateWithMonocularSaccades(pptrials, trialIndex, saccadePairs)
run('saccadeMatrixIndex.m');

for EYE = 1:2
    
    switch EYE
        case 1
            ss = saccadePairs(:, EYE1_START);
            ee = saccadePairs(:, EYE1_STOP);
        case 2
            ss = saccadePairs(:, EYE2_START);
            ee = saccadePairs(:, EYE2_STOP);
    end
    
    ss(isnan(ss)) = [];
    ee(isnan(ee)) = [];
    
    for ii = 1:length(ss)
        if ss(ii) == 1
            continue;
        end
        if any(pptrials{trialIndex, EYE}.microsaccades.start == ss(ii))
            continue;
        end
        
        % add new microsaccade event
        ms = pptrials{trialIndex, EYE}.microsaccades;
        ms = addEvents(ms, ss(ii), ee(ii) - ss(ii));
        ms = updateAmplitudeAngle(pptrials{trialIndex, EYE}, ms);
        
        pptrials{trialIndex, EYE}.microsaccades = ms;
        
        pptrials{trialIndex, EYE} = findDrifts(pptrials{trialIndex, EYE});  
    end
    
end
end