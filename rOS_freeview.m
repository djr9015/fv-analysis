% simple subject-running code. Modified from Janis's runOneSubject.m
close all
%% load in data
subject = 'MAC';

pathtodata = "C:\Users\ryckm_000\Documents\MATLAB\Freeview\FV-analysis\RawDataCopy";
 
load(fullfile(pathtodata, sprintf('%s_pptrials.mat', subject)), 'pptrials', 'data');

load('C:\Users\ryckm_000\Documents\MATLAB\Freeview\FV-analysis\RawDataCopy\MAC_pptrials.mat');

Fs = 1000; %sample rate
%% subject specific fixes
switch subject
    case 'MAC'
        pptrials(1, :) = []; % something wrong with first trial?
    otherwise
        
end

%% remove saccade overshoots
pptrials_orig = pptrials;
pptrials(:, 1) = osRemoverInEM(pptrials(:, 1));
pptrials(:, 2) = osRemoverInEM(pptrials(:, 2));

%% prepare for figures
imgPath = "C:\Users\ryckm_000\Documents\MATLAB\Freeview\FV-analysis\figures";

%% apparatus and room parameters
monitorWidth = 508;
monitorWidthP = 1920;

dist2monitor = 4235; % JI: I think??????

pixel2angle = pptrials{end, 1}.PixelAngle;

% for now estimate some parameters
p = 60; % interpupillary distance (IPD)
r_L = [0, -p/2, 0]'; % position vectors of eyes in space
r_R = [0, p/2, 0]';
r_mon = [dist2monitor, 0, 0]';
baseline = r_R - r_L;
r_cyc = (r_R + r_L) / 2;

% angular offsets from straight ahead for each
theta_offset(1, 1) = atan2( r_mon(2) - r_R(2), r_mon(1) - r_R(1) );
theta_offset(1, 2) = atan2( r_mon(3) - r_R(3), r_mon(1) - r_R(1) );
theta_offset(2, 1) = atan2( r_mon(2) - r_L(2), r_mon(1) - r_L(1) );
theta_offset(2, 2) = atan2( r_mon(3) - r_L(3), r_mon(1) - r_L(1) );

%% RUN PARAMETERS
MAX_INVALID_DURATION = 400; % ms - limit on blink / no tracks

nanflds = {'blinks', 'notracks'};
emflds = {'drifts', 'microsaccades', 'saccades'};
emlabels = {'d', 'ms', 's'};

params = struct();
params.tasks = {'freeview'};
params.startTimes = {'start'};
params.stopTimes = {'samples'};
params.taskColors = {'k'};
params.taskEyeColors = {[1, 0, 0], [240, 128, 128]/255;...
                        [0, 100, 0]/255, [107, 142, 35]/255; ...
                        'b', [40, 60, 80]/100;
                        'k', [.5, .5, .5]};
params.plottasks = 0; % -1 from normal indexing

params.emfilters = {'driftonly', 'msallowed', 'saccadeallowed',...
    'msonly', 'saccadeonly', 'all'}; % analyze with this filters

% histogram limits / bins for drift parameters
params.nbins = [100, 30]; % instantaneous and average number of bins

params.drift.dur = linspace(100, 1500, 30);
params.drift.pos = linspace(-5, 5, 30);
params.drift.speed_lims = [0, 100];
params.drift.vel_lims = [-100, 100];
params.drift.curv_lims = [0, 80];
params.drift.vergence_pos = {linspace(-40, 40, 80), linspace(-20, 20, 40)};
params.drift.vergence_vel_lims = [-200, 200];
params.drift.vergence_change = {linspace(-40, 40, 80), linspace(-20, 20, 40)};

params.saccade.amp = linspace(0, 300, 30); % originally (0, 60, 30)
params.saccade.dir = linspace(0, 2*pi, 30);
params.saccade.pkvel = linspace(0, 20, 30);
params.saccade.amp_diff = linspace(-20, 20, 30);
params.saccade.V_amp = linspace(-15, 15, 30);
params.saccade.pkvel_diff = linspace(-30, 30, 30);
params.saccade.S_ampxy = linspace(-120, 120, 30); % originally (-80, 80, 30)
params.saccade.S_amp = linspace(0, 80, 30);

%% compute instantaneous eye movements
% compute vergence, binocular gaze point, velocity, curvature, ... for
% downstream use

%uses processInstantaneousEyeMovementsFV, which is modified to run with
%only one 'task' and fewer paramters than the original function
fname = sprintf('%s_pptrials_processed.mat', subject);
if ~exist(fullfile(pathtodata, fname), 'file')
    pptrials = processInstantaneousEyeMovementsFV(...
        pptrials, pptrials_orig, pixel2angle, dist2monitor, ...
        baseline, r_cyc, theta_offset);
    save(fullfile(pathtodata, fname), 'pptrials');
    else
    fprintf('loading %s\n', fname);
    load(fullfile(pathtodata, fname));
end
%% trial counts
switch subject
    case 'MAC'
        for ii = 1:length(pptrials)
            pptrials{ii, 1}.responseFused = 1;
            pptrials{ii, 2}.responseFused = 1;
        end
end

counts = countTrialsFV(pptrials);
%% Monitor size
for i = 1:length(pptrials)
    pptrials{i,1}.MonitorAngle = pptrials{i,1}.PixelAngle*monitorWidthP/60;
end

%% Separate saccades and drift - binocular
% FV function indicates changes that allow it to run with only one task
fname = sprintf('%s_pptrials_segmented.mat', subject);
if ~exist(fullfile(pathtodata, fname), 'file')
    pptrials = segmentBinocularEMFV(pptrials, counts, params);
    save(fullfile(pathtodata, fname), 'pptrials');
else
    fprintf('loading %s\n', 'pptrials_segmented_.mat');
    load(fullfile(pathtodata, fname));
end

%% Analysis
% add start field, fix VS components
for channel = 1:2
    for j = 1:length(pptrials)
        pptrials{j, channel}.start = 1;
    end
end
for i = 1:length(pptrials)
    pptrials{i, 1}.vs_components = pptrials{i, 1}.vs_components/2; %should have been divided by 2 earlier?
end

% Drifts
%{
driftout = analyzeDrifts(pptrials, counts, params, true, imgPath);
driftout3d = analyzeDrifts_3d(pptrials, counts, params, true, imgPath);
%}
% Saccades
%{
saccadeout = analyzeSaccades(pptrials, counts, params, true, imgPath);
saccadeout3d = analyzeSaccades_3d(pptrials, counts, params, true, imgPath);
%}
% Diffusion
%{
nboots = 10;
warning off %to suppress "X is rank deficient to within machine precision"
diffusionout = analyzeDriftDiffusions(pptrials, counts, nboots, params, true, imgPath);
warning on
%}

%% Locating possible vergence saccade pairs
%{
vergenceSaccades = [];
for trial = 1:length(pptrials)
    if isfield(pptrials{trial,1}, 'saccadePairs') 
        for sacIdx = 1:size(pptrials{trial,1}.saccadePairs,1)
            if (pptrials{trial,1}.saccadePairs(sacIdx,7) > deg2rad(300) && pptrials{trial,1}.saccadePairs(sacIdx,7) < deg2rad(400)...
                    && pptrials{trial,1}.saccadePairs(sacIdx,8) > deg2rad(150) && pptrials{trial,1}.saccadePairs(sacIdx,8) < deg2rad(250))%more elegant?
            vergenceSaccades = vertcat(vergenceSaccades, [pptrials{trial,1}.saccadePairs(sacIdx, :), trial, sacIdx]);
            end
        end
    end
end
scatter(vergenceSaccades(:,7),vergenceSaccades(:,8))
%}

%% L/R Drift Characteristics
% driftCharsLR loops through all drifts in each channel and saves them to
% drifts structure to be used in diffusion calculation
nBins = 4;

driftsLR = driftCharsLR(pptrials);
[driftbinsR, E_R] = binDriftsByVergence(driftsLR{1}, nBins);
[driftbinsL, E_L] = binDriftsByVergence(driftsLR{2}, nBins);

%% Binocular Drift Characteristics
% driftChanrsVS loops through drifts, trimming drifts as in binocular fn 
% analyzeDrifts then saves drifts to structure using version as x and y
driftsV = driftCharsVS(pptrials, params);
% plot drift characteristics as a function of vergence and print p-val
%{
figpath = "C:\Users\ryckm_000\Documents\MATLAB\Freeview\FV-analysis\figures\driftCharacteristics";
close all
dim = [.2 .5 .3 .3];
figure
scatter([drifts.mean_vergence], [drifts.span], 5, 'filled', 'k')
lsline
xlabel('vergence')
ylabel('span')
title('span')
[Rspan, Pspan] = corrcoef([drifts.mean_vergence], [drifts.span], 'Rows', 'complete');
str = sprintf('p = %f',Pspan(1,2));
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftspan_corr.png'))

figure
scatter([drifts.mean_vergence], [drifts.speed], 5, 'filled', 'k')
lsline
title('speed')
xlabel('vergence')
ylabel('speed')
[Rspeed, Pspeed] = corrcoef([drifts.mean_vergence], [drifts.speed], 'Rows', 'pairwise');
str = sprintf('p = %f',Pspeed(1,2));
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftspeed_corr.png'))

figure
scatter([drifts.mean_vergence], [drifts.vary], 5, 'filled', 'k')
lsline
title('vert. variance')
xlabel('vergence')
ylabel('variance (y)')
[Rvary, Pvary] = corrcoef([drifts.mean_vergence], [drifts.vary], 'Rows', 'pairwise');
str = sprintf('p = %f',Pvary(1,2));
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftvary_corr.png'))

figure
scatter([drifts.mean_vergence], filloutliers([drifts.varx], 'nearest', 'mean'), 5, 'filled', 'k')
lsline
title('horiz. variance')
xlabel('vergence')
ylabel('variance (x)')
[Rvarx, Pvarx] = corrcoef([drifts.mean_vergence], [drifts.varx], 'Rows', 'pairwise');
str = sprintf('p = %f',Pvarx(1,2));
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftvarx_corr.png'))

figure
scatter([drifts.mean_vergence], filloutliers([drifts.curv], 'nearest', 'mean'), 5, 'filled', 'k')
lsline
title('curvature')
xlabel('vergence')
ylabel('curvature')
[Rcurv, Pcurv] = corrcoef([drifts.mean_vergence], [drifts.curv], 'Rows', 'pairwise');
str = sprintf('p = %f',Pcurv(1,2));
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftcurv_corr.png'))

figure
scatter([drifts.mean_vergence], [drifts.duration], 7, 'filled', 'k')
lsline
title('duration')
xlabel('vergence')
ylabel('duration')
[Rduration, Pduration] = corrcoef([drifts.mean_vergence], [drifts.duration], 'Rows', 'pairwise');
str = sprintf('p = %f',Pduration(1,2));
annotation('textbox',dim,'String',str,'FitBoxToText','on');
saveas(gcf,fullfile(figpath,'driftduration_corr.png'))

%}

%% Binocular Diffusion By Vergence
warning off
% Equally spaced vergence bins (doesn't have the same # of drifts per bin)
%{
[Y, E] = discretize([drifts.mean_vergence], nBins);
driftBins = cell(nBins,1);
for i = 1:nBins
    driftBins{i} = drifts(Y==i);
end
%}


% Bin by number of drifts
[driftbinsV, E_V] = binDriftsByVergence(driftsV, nBins);

% Plot diffusion constants
%{
for i = 1:nBins
    [~, ~, dc, ~, Dsq, SingleSegmentDsq] = ...
        CalculateDiffusionCoef (driftbinsVS{i});
    
    fprintf('Diffusion constant (vergence: %1.0f to %1.0f) = %1.2f arcmin^2/s\n', ...
        vergE_vs(i), vergE_vs(i+1), dc);

    tdsq = (1:length(Dsq)) / Fs * 1000; % ms

    figure; clf; % plot information about drift diffusion
    sgtitle(sprintf('diffusion data (vergence: %1.1f to %1.1f)', ...
        vergE_vs(i), vergE_vs(i+1)));
    subplot(1, 2, 1);
    plot(tdsq, SingleSegmentDsq, 'lineWidth', .25, 'linestyle', '-');
    xlim(tdsq([1, end]));
    xlabel('time lag (ms)');
    ylabel('variance of displacement (arcmin^2)');
    title('single segments');

    subplot(1, 2, 2); hold on;
    plot(tdsq, Dsq, 'k-', 'linewidth', 2);
    plot(tdsq, (4 * tdsq) * dc / Fs, 'b--', 'linewidth', 2)
    xlim(tdsq([1, end]));
    xlabel('time lag (ms)');
    ylabel('variance of displacement (arcmin^2)');
    legend({'data', 'brownian motion'}, 'Location', 'northwest');
    title('all drifts');    
end
%}

% Vergence drifts
% copy vergence from binocular drifts structure to x/y in the same
% structure and save it as a new structure
driftbinsS = driftbinsV;
for i = 1:nBins
    for ii = 1:length(driftbinsS{i,1})
        driftbinsS{i,1}(ii).x = (driftbinsS{i,1}(ii).vergx)';
        driftbinsS{i,1}(ii).y = (driftbinsS{i,1}(ii).vergy)';
    end
end
%% Diffusion constants by vergence and type
% Dc's of LRVS
%{
warning off
diffconstsR = zeros(2,nBins); %in order R, L, V, S
diffconstsL = zeros(2,nBins); 
diffconstsV = zeros(2,nBins);
diffconstsS = zeros(2,nBins);

figure
hold on
for i = 1:nBins
    [~, ~, dcR, ~, dsqR, ~] = CalculateDiffusionCoef(driftbinsR{i});
    diffconstsR(1,i) = (E_R(i)+E_R(i+1))/2;
    diffconstsR(2,i) = dcR;
    plot(dsqR)
end
title('Dsq (Right)')
legend('location', 'northwest')

figure
hold on
for i = 1:nBins
    [~, ~, dcL, ~, dsqL, ~] = CalculateDiffusionCoef(driftbinsL{i});
    diffconstsL(1,i) = (E_L(i)+E_L(i+1))/2;
    diffconstsL(2,i) = dcL;
    plot(dsqL)
end
title('Dsq (Left)')
legend('location', 'northwest')

figure
hold on
for i = 1:nBins
    [~, ~, dcV, ~, dsqV, ~] = CalculateDiffusionCoef(driftbinsV{i});
    diffconstsV(1,i) = (E_V(i)+E_V(i+1))/2;
    diffconstsV(2,i) = dcV;
    plot(dsqV)
end
title('Dsq (Version)')
legend('location', 'northwest')

figure
hold on
for i = 1:nBins
    [~, ~, dcS, ~, dsqS, ~] = CalculateDiffusionCoef(driftbinsS{i});
    diffconstsS(1,i) = (E_V(i)+E_V(i+1))/2;
    diffconstsS(2,i) = dcS;
    plot(dsqS)
end
title('Dsq (Vergence)')
legend('location', 'northwest')
%}

% Plot dc's
%{
figure
hold on
plot(diffconstsR(1,:),diffconstsR(2,:), '->', 'displayname', 'Right')
plot(diffconstsL(1,:),diffconstsL(2,:), '-<', 'displayname', 'Left')
plot(diffconstsV(1,:),diffconstsV(2,:), '-d', 'displayname', 'Version')
plot(diffconstsS(1,:),diffconstsS(2,:), '-s', 'displayname', 'Vergence')
xlabel('Vergence')
ylabel('Diffusion Constant (acrmin^2/s)')
title('Diffusion Constants by Vergence')
legend
%}

%% Diffusion constant boostrapping
%{
tic
nBoots = 20;
warning off
diffconstsR = zeros(2,nBins, nBoots); %in order R, L, V, S
diffconstsL = zeros(2,nBins, nBoots); 
diffconstsV = zeros(2,nBins, nBoots);
diffconstsS = zeros(2,nBins, nBoots);

fprintf('Calculating right diffusion constant...\n')
for i = 1:nBins
    for bi = 1:nBoots
        rs = randsample(driftbinsR{i}, length(driftbinsR{i}), true);
        [~, ~, dcR, ~, dsqR, ~] = CalculateDiffusionCoef(rs);
        diffconstsR(1,i,bi) = (E_R(i)+E_R(i+1))/2;
        diffconstsR(2,i,bi) = dcR;
    end
end

fprintf('Calculating left diffusion constant...\n')
for i = 1:nBins
    for bi = 1:nBoots
        rs = randsample(driftbinsL{i}, length(driftbinsL{i}), true);
        [~, ~, dcL, ~, dsqL, ~] = CalculateDiffusionCoef(rs);
        diffconstsL(1,i,bi) = (E_L(i)+E_L(i+1))/2;
        diffconstsL(2,i,bi) = dcL;
    end
end

fprintf('Calculating version diffusion constant...\n')
for i = 1:nBins
    for bi = 1:nBoots
        rs = randsample(driftbinsV{i}, length(driftbinsV{i}), true);
        [~, ~, dcV, ~, dsqV, ~] = CalculateDiffusionCoef(rs);
        diffconstsV(1,i,bi) = (E_V(i)+E_V(i+1))/2;
        diffconstsV(2,i,bi) = dcV;
    end
end

fprintf('Calculating vergence diffusion constant...\n')
for i = 1:nBins
    for bi = 1:nBoots
        rs = randsample(driftbinsS{i}, length(driftbinsS{i}), true);
        [~, ~, dcS, ~, dsqS, ~] = CalculateDiffusionCoef(rs);
        diffconstsS(1,i,bi) = (E_V(i)+E_V(i+1))/2;
        diffconstsS(2,i,bi) = dcS;
    end
end
toc

figure
hold on
errorbar(diffconstsR(1,:,1), mean(diffconstsR(2,:,:), 3), std(diffconstsR(2,:,:), 0, 3)/sqrt(nBoots), '->', 'displayname', 'Right')
errorbar(diffconstsL(1,:,1), mean(diffconstsL(2,:,:), 3), std(diffconstsR(2,:,:), 0, 3)/sqrt(nBoots), '-<', 'displayname', 'Left')
errorbar(diffconstsV(1,:,1), mean(diffconstsV(2,:,:), 3), std(diffconstsR(2,:,:), 0, 3)/sqrt(nBoots), '-d', 'displayname', 'Version')
errorbar(diffconstsS(1,:,1), mean(diffconstsS(2,:,:), 3), std(diffconstsR(2,:,:), 0, 3)/sqrt(nBoots), '-s', 'displayname', 'Vergence')
xlabel('Vergence')
ylabel('Diffusion Constant (acrmin^2/s)')
boottitle = sprintf('Diffusion Constants by Vergence (%d boots)', nBoots);
sgtitle(boottitle)
legend
%}

%% Depth-Vergence across all trials
%%{
pixel2angle = pptrials{end, 1}.PixelAngle;
resScale = 720/1080;
angle2img = resScale/pixel2angle;
imgpath = "C:\Users\ryckm_000\Documents\MATLAB\Freeview\images\";

% choose size over which to average
avgradius = 30; %arcmin
pixrad = ceil(avgradius/pixel2angle);
allverg = [];
alldepth = [];
for trialIdx = 1:length(pptrials)

    x_plot = pptrials{trialIdx,1}.x.position*angle2img;
    y_plot = -pptrials{trialIdx,1}.y.position*angle2img;

    rangename = sprintf('rRange%03d.mat', pptrials{trialIdx,1}.ImageNum);
    load(fullfile(imgpath, rangename))

    trialdepth = zeros(1, length(x_plot));
    x_pix = ceil(x_plot+1280/2);
    y_pix = ceil(y_plot+720/2);
    for i = 1:length(x_plot)
        inrangeY = y_pix(i)-pixrad >= 1 & y_pix(i)+pixrad <= size(rangeImg,1);
        inrangeX = x_pix(i)-pixrad >= 1 & x_pix(i)+pixrad <= size(rangeImg,2);
        if ~(inrangeY && inrangeX)
            continue
        end
        trialdepth(i) = mean(rangeImg(y_pix(i)-pixrad:y_pix(i)+pixrad, x_pix(i)-pixrad:x_pix(i)+pixrad), 'all');
    end
    
    sacEnd = pptrials{trialIdx,1}.saccades.start + pptrials{trialIdx,1}.saccades.duration;
    trialdepthSE = trialdepth(sacEnd);
    trialvergSE = pptrials{trialIdx,1}.vs_components(sacEnd, 3)';
    alldepth = [alldepth, trialdepthSE];
    allverg = [allverg, trialvergSE];
end
figure
scatter(allverg, alldepth, 3)
xlabel('Vergence [arcmin]')
ylabel('Depth [m]')
title('Post-saccadic Vergence vs. Depth')
%}

%% 
warning on