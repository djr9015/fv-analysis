function [] = visualize3dFV(datafileMAT, trialIdx, figureSaveDirectory)
%   plots and saves the bgp, HV traces, and VS components of a given free
%   viewing trial from a .mat file
%% load in a file from free viewing task
load(datafileMAT, 'pptrials');% load in variable 'pptrials' from mat file
Fs = 1000; %sample rate
close all
%% 3d plot
bgp = pptrials{trialIdx,1}.r_bgp_int;

bgp_plot = figure(1);
C = jet(length(bgp(1,:)));
scatter3(bgp(1,:), bgp(2,:), bgp(3,:), 5, C);
xlabel('z')
ylabel('x')
zlabel('y')
text(double(bgp(1,1)), double(bgp(2,1)), double(bgp(3,1)), '\leftarrow Start');
fname1 = sprintf('bgp_trial%d.png', trialIdx);
saveas(bgp_plot, fullfile(figureSaveDirectory,fname1));

%% 2d H/V plot
HVtrace = figure(2);
subplot(2,1,1)
hold on
xlabel('time (ms)');
ylabel('position (arcmin)');
legend('location', 'best')
plot(pptrials{trialIdx,1}.x.position, '-r', 'displayName', 'Right x')
plot(pptrials{trialIdx,2}.x.position, '-b', 'displayName', 'Left x')
subplot(2,1,2)
hold on
xlabel('time (ms)');
ylabel('position (arcmin)');
legend('location', 'best')
plot(pptrials{trialIdx,1}.y.position, '-r', 'displayName', 'Right y')
plot(pptrials{trialIdx,2}.y.position, '-b', 'displayName', 'Left y')

% Highlight saccades
yl = ylim;
issaccadeR = ~isempty(pptrials{trialIdx, 1}.saccades.start);
issaccadeL = ~isempty(pptrials{trialIdx, 2}.saccades.start);
if (issaccadeL && issaccadeR)
    saccadestart = pptrials{trialIdx, 1}.saccades.start(1);
    saccadestop = pptrials{trialIdx, 1}.saccades.duration(1) + saccadestart - 1;
    
    patch([saccadestart, saccadestop, saccadestop, saccadestart],...
        [yl(1), yl(1), yl(2), yl(2)],...
        [.7 .2 .2],...
        'EdgeColor', 'none',...
        'FaceAlpha', .4, 'displayName', 'saccade');
end

% highlight microsaccades
ismicrosaccadeR = ~isempty(pptrials{trialIdx, 1}.microsaccades.start);
ismicrosaccadeL = ~isempty(pptrials{trialIdx, 2}.microsaccades.start);
if (ismicrosaccadeL && ismicrosaccadeR)
    microsaccadestart = pptrials{trialIdx}.microsaccades.start(1);
    microsaccadestop = pptrials{trialIdx}.microsaccades.duration(1) + microsaccadestart - 1;
    
    patch([microsaccadestart, microsaccadestop, microsaccadestop, microsaccadestart],...
        [yl(1), yl(1), yl(2), yl(2)],...
        [.2 .2 .7],...
        'EdgeColor', 'none',...
        'FaceAlpha', .4, 'displayName', 'microsaccade');
end

fname2 = sprintf('HVtrace_trial%d.png', trialIdx);
saveas(HVtrace, fullfile(figureSaveDirectory,fname2));

%% Version and vergence
VScomponents = figure(3);
subplot(2,1,1)
hold on
plot(pptrials{trialIdx,1}.vs_components(:,1), '-r', 'displayName', 'Version x')
plot(pptrials{trialIdx,1}.vs_components(:,2), '-g', 'displayName', 'Version y')
legend('location', 'best')
subplot(2,1,2)
hold on
yline(0, 'HandleVisibility', 'off');
plot(pptrials{trialIdx,1}.vs_components(:,2+1), '-b', 'displayName', 'Vergence x')
plot(pptrials{trialIdx,1}.vs_components(:,4), '-k', 'displayName', 'Vergence y')
legend('location', 'best')

fname3 = sprintf('VScomponents_trial%d.png', trialIdx);
saveas(VScomponents, fullfile(figureSaveDirectory,fname3));

%% Right/Left XY-trace on image
pixel2angle = pptrials{end, 1}.PixelAngle;
resScale = 720/1080;
angle2img = resScale/pixel2angle;
imgpath = "C:\Users\ryckm_000\Documents\MATLAB\Freeview\images\";

% Right
imgname = sprintf('rImage%03dV.bmp', pptrials{trialIdx,1}.ImageNum);
img = imread(fullfile(imgpath, imgname));
imageTraceR = figure(4);
imshow(img)
[rows, columns, ~] = size(img);
origin = [columns/2, rows/2];
xdata = -origin(1) : columns - origin(1);
ydata = -origin(2) : rows - origin(2);
imshow(img, 'XData', xdata, 'YData', ydata)
grid on
hold on
x_plot = pptrials{trialIdx,1}.x.position*angle2img;
y_plot = -pptrials{trialIdx,1}.y.position*angle2img;
color = jet(length(x_plot));
clim = [0, length(x_plot)/Fs];
scatter(x_plot, y_plot, 5, color)
% text(x_plot(1)),y_plot(1), '\leftarrow Start', 'color', 'white')
colormap(color)
caxis(clim)
h = colorbar('westoutside');
ylabel(h, 'Time [s]')

fname4 = sprintf('imageTraceR_trial%d.png', trialIdx);
saveas(imageTraceR, fullfile(figureSaveDirectory, fname4));

%Left
imgname = sprintf('lImage%03dV.bmp', pptrials{trialIdx,1}.ImageNum);
img = imread(fullfile(imgpath, imgname));
imageTraceL = figure(5);
imshow(img)
[rows, columns, ~] = size(img);
origin = [columns/2, rows/2];
xdata = -origin(1) : columns - origin(1);
ydata = -origin(2) : rows - origin(2);
imshow(img, 'XData', xdata, 'YData', ydata)
grid on
hold on
x_plot = pptrials{trialIdx,2}.x.position*angle2img;
y_plot = -pptrials{trialIdx,2}.y.position*angle2img;
color = jet(length(x_plot));
clim = [0, length(x_plot)/Fs];
scatter(x_plot, y_plot, 5, color)
% text(x_plot(1,1),y_plot(1,1), '\leftarrow Start', 'color', 'white')
colormap(color)
caxis(clim)
h = colorbar('westoutside');
ylabel(h, 'Time [s]')

fname5 = sprintf('imageTraceL_trial%d.png', trialIdx);
saveas(imageTraceL, fullfile(figureSaveDirectory, fname5));

%% Depth mapping

% right
rangename = sprintf('rRange%03d.mat', pptrials{trialIdx,1}.ImageNum);
load(fullfile(imgpath, rangename))
depthmapR = figure(6);
[rows, columns] = size(rangeImg);
origin = [columns/2, rows/2];
xdata = -origin(1) : columns - origin(1);
ydata = -origin(2) : rows - origin(2);
imagesc(rangeImg, 'XData', xdata, 'YData', ydata);
grid on
hold on
x_plot = pptrials{trialIdx,1}.x.position*angle2img;
y_plot = -pptrials{trialIdx,1}.y.position*angle2img;
scatter(x_plot, y_plot, 5, 'k')
h = colorbar('westoutside');
ylabel(h, 'Depth [m]')
daspect([1 1 1])

fname6 = sprintf('depthmapR_trial%d.png', trialIdx);
saveas(depthmapR, fullfile(figureSaveDirectory, fname6));

% left
rangename = sprintf('lRange%03d.mat', pptrials{trialIdx,1}.ImageNum);
load(fullfile(imgpath, rangename))
depthmapL = figure(7);
[rows, columns] = size(rangeImg);
origin = [columns/2, rows/2];
xdata = -origin(1) : columns - origin(1);
ydata = -origin(2) : rows - origin(2);
imagesc(rangeImg, 'XData', xdata, 'YData', ydata);
grid on
hold on
x_plot = pptrials{trialIdx,2}.x.position*angle2img;
y_plot = -pptrials{trialIdx,2}.y.position*angle2img;
scatter(x_plot, y_plot, 5, 'k')
h = colorbar('westoutside');
ylabel(h, 'Depth [m]')
daspect([1 1 1])

fname7 = sprintf('depthmapL_trial%d.png', trialIdx);
saveas(depthmapL, fullfile(figureSaveDirectory, fname7));
%% Vergence comparison to depth

% choose size over which to average
avgradius = 45; %arcmin, fovea size (radius, so 0.75 visual degrees)
pixrad = ceil(avgradius/pixel2angle);

depth = zeros(1, length(x_plot));
x_pix = ceil(x_plot+1280/2);
y_pix = ceil(y_plot+720/2);
for i = 1:length(x_plot)
    inrangeY = y_pix(i)-pixrad >= 1 & y_pix(i)+pixrad <= size(rangeImg,1);
    inrangeX = x_pix(i)-pixrad >= 1 & x_pix(i)+pixrad <= size(rangeImg,2);
    if ~(inrangeY && inrangeX)
        continue
    end
    depth(i) = mean(rangeImg(y_pix(i)-pixrad:y_pix(i)+pixrad, x_pix(i)-pixrad:x_pix(i)+pixrad), 'all');
end

vergdepth = figure(8);
hold on
plot(pptrials{trialIdx,1}.vs_components(:, 3), 'displayname', 'Vergence')
plot(depth, 'displayname', 'Depth of scene [m]')
legend('location', 'best')
xlabel('Samples (1000/sec)')

fname8 = sprintf('vergdepth_trial%d.png', trialIdx);
saveas(vergdepth, fullfile(figureSaveDirectory, fname8));
end