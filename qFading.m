pathtodata = "C:\Users\ryckm_000\Documents\MATLAB\Freeview\Freeview MATLAB\RawDataCopy";

slec = eis_readData_binocular([], 'x1');
slec = eis_readData_binocular(slec, 'y1');
slec = eis_readData_binocular(slec, 'x2');
slec = eis_readData_binocular(slec, 'y2');

slec = eis_readData_binocular(slec, 'stream', 0, 'int'); % right trigger
slec = eis_readData_binocular(slec, 'stream', 1, 'int'); % left trigger

slec = eis_readData_binocular(slec, 'stream', 2, 'double'); % stabalized eye 1 data x1.y1
slec = eis_readData_binocular(slec, 'stream', 3, 'double');  % stabalized eye 2 data x1.y1

slec = eis_readData_binocular(slec, 'trigger', 'blink1');
slec = eis_readData_binocular(slec, 'trigger', 'notrack1');
slec = eis_readData_binocular(slec, 'trigger', 'blink2');
slec = eis_readData_binocular(slec, 'trigger', 'notrack2');

slec = eis_readData_binocular(slec,'photocell');
slec = eis_readData_binocular(slec,'spf');

slec = eis_readData_binocular(slec, 'uservar','SubjectName');
slec = eis_readData_binocular(slec, 'uservar','DEBUG');
slec = eis_readData_binocular(slec, 'uservar','Trial');

slec = eis_readData_binocular(slec, 'uservar','StabCond');
slec = eis_readData_binocular(slec, 'uservar','EyeCond');

slec = eis_readData_binocular(slec, 'uservar','RecalFrequency');
slec = eis_readData_binocular(slec, 'uservar','RecalIncrement');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetX1');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetY1');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetX2');
slec = eis_readData_binocular(slec, 'uservar','RecalOffsetY2');

slec = eis_readData_binocular(slec, 'uservar','PixelAngle');



%[data, files] = readdata(pathtodata, slec, true); % this is the raw read in of the data
data = readdata(pathtodata, slec);
%%
 info.StabCond = {'Stabilized','Right Eye Stabilized','Left Eye Stabilized','Normal'};
 info.EyeCond   = {'Binocular','RightEye','LeftEye'};
 info.RightStabTrace = 'stream02'; % x.y
 info.LeftStabTrace  = 'stream03'; % x.y
 info.StabTraceType  = 'float';
 info.RightEyeIdx    = 1;
 info.LeftEyeIdx     = 2;
 info.codes          = getEventCodes();


em  = preprocessBinocularEM(data);

%%

for tr = 1:length(data.user)
    
    COND(tr,:) = [data.user{tr}.EyeCond data.user{tr}.StabCond];
    
    RT(tr,:) = any(data.stream00{tr}.data == 7); 
    LT(tr,:) = any(data.stream01{tr}.data == 7);
    
    FadeTM(tr,:) = data.stream00{tr}.ts(find(data.stream00{tr}.data == 7,1)); 
    
end
%%

figure('units','in','position',[0 0 1 3/4 ] *7)
clf
for p = 1:2

    if p == 1
ystr =  'Percent Total Fading'; 
DATA = RT; 

    elseif p == 2
    ystr =  'Percent Partial Fading'; 
DATA = LT; 
    end

eyes = [1 2]; clear u
for eye = 0:2
    
    if eye ~= 0
    
        normal =  COND(:,1) == eye & ...
           (COND(:,2) == 3 | COND(:,2) == eyes(eyes~=eye));
       
        stab = COND(:,1) == eye & ...
           (COND(:,2) == eye | COND(:,2) == 0);
      
       u(eye+1,1) =  mean(DATA(normal));
       u(eye+1,2) =  mean(DATA(stab)); 
    
       n(eye+1,1) =  numel(DATA(normal));
       n(eye+1,2) =  numel(DATA(stab)); 
       
    else
         normal =  COND(:,1) == eye & ...
           (COND(:,2) == 3) ;
       
        stab = COND(:,1) == eye & ...
           ( COND(:,2) == 0);
      
       u(eye+1,1) =  mean(DATA(normal));
       u(eye+1,2) =  mean(DATA(stab)); 
       
       n(eye+1,1) =  numel(DATA(normal));
       n(eye+1,2) =  numel(DATA(stab)); 
    end
end

subplot(1,2,p)
bar(u*100); hold on
 set(gca,'box','off','tickdir','out');
 set(gca,'XtickLabel',{'Binocular','RightEye','LeftEye'})
 legend('Normal','Stabilized','Location','best')
 ylabel(ystr); 
 ylim([0 100])
 for eye = 1:3
     for stab = 1:2
 text(eye + -0.2 + (stab-1)/4,0, sprintf('%u',n(eye,stab)),...
     'VerticalAlignment','bottom')
     end
 end
 text(min(xlim),5,'Trial N =','VerticalAlignment','bottom','HorizontalAlignment','Left')
 title(sprintf('MAC Fading Report\n%s',ystr(9:end)))
end
    
    
    
    
